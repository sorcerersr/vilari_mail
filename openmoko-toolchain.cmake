#http://www.cmake.org/Wiki/CMake_Cross_Compiling
#run with  cmake -DCMAKE_TOOLCHAIN_FILE=openmoko-toolchain.cmake -DCMAKE_INSTALL_PREFIX=/usr



INCLUDE(CMakeForceCompiler)

# this one is important
SET(CMAKE_SYSTEM_NAME arm-angstrom-linux-gnueabi)
#this one not so much
SET(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler

SET(CMAKE_C_COMPILER   /usr/local/openmoko/arm/bin/arm-angstrom-linux-gnueabi-gcc)

SET(CMAKE_CXX_COMPILER /usr/local/openmoko/arm/bin/arm-angstrom-linux-gnueabi-gcc)

CMAKE_FORCE_C_COMPILER(arm-angstrom-linux-gnueabi-gcc GNU)
CMAKE_FORCE_CXX_COMPILER(arm-angstrom-linux-gnueabi-gcc GNU)


# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH  /usr/local/openmoko/arm/arm-angstrom-linux-gnueabi)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)