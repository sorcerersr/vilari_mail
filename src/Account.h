/*
 *      Account.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <commons.h>

class Account
{
    public:
        Account();
        virtual ~Account();


        void setName(std::string name);
        std::string getName();

        void setType(std::string type);
        std::string getType();

        void setServer(std::string server);
        std::string getServer();

        void setPort(uint port);
        uint getPort();

        void setUsername(std::string username);
        std::string getUsername();

        void setPassword(std::string password);
        std::string getPassword();

        void setSSL(bool usessl);
        bool useSSL();

        void setDefault(bool isdefault);
        bool isDefault();


        std::string toString();

    protected:
    private:

        std::string m_name;
        std::string m_type;
        std::string m_server;
        uint m_port;
        std::string m_username;
        std::string m_password;
        bool m_ssl;
        bool m_default;
};

#endif // ACCOUNT_H
