/*
 *      Address.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADRESS_H
#define ADRESS_H

#include <commons.h>

class Address
{
    public:
        Address();
        virtual ~Address();

        void setDisplayName(std::string displayname);
        std::string getDisplayName();

        void setAddress(std::string address);
        std::string getAddress();

    protected:
    private:
        std::string m_displayname;
        std::string m_address;
};

#endif // ADRESS_H
