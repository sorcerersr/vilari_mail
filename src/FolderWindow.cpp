/*
 *      FolderWindow.cpp
 *
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FolderWindow.h"

FolderWindow::FolderWindow(Evas_Object* win)  : Content(win)
{
    m_selected_folder = NULL;

    Evas_Object* verbox = elm_box_add(win);
    evas_object_size_hint_weight_set(verbox, 1.0, 1.0);
    evas_object_size_hint_align_set(verbox, -1.0, -1.0);
    elm_box_pack_end(m_content_box, verbox);
    evas_object_show(verbox);



    m_folderlist = elm_list_add(win);
    elm_box_pack_end(verbox, m_folderlist);
    evas_object_size_hint_weight_set(m_folderlist, 1.0, 1.0);
    evas_object_size_hint_align_set(m_folderlist, -1.0, -1.0);

    elm_scroller_bounce_set(m_folderlist ,false, false);


    elm_list_go(m_folderlist);
    evas_object_show(m_folderlist);

    Evas_Object* horbx = elm_box_add(win);
    elm_box_horizontal_set(horbx, true);
    evas_object_size_hint_weight_set(horbx, 1.0, 0.0);
    evas_object_size_hint_align_set(horbx, -1.0, -1.0);
    elm_box_pack_end(m_content_box, horbx);
    evas_object_show(horbx);


    Evas_Object* close_bt = elm_button_add(win);
    elm_button_label_set(close_bt, "Close");
    evas_object_smart_callback_add(close_bt, "clicked", close_action, NULL);
    evas_object_size_hint_weight_set(close_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(close_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, close_bt);
    evas_object_show(close_bt);


    m_actions_bt = elm_button_add(win);
    elm_button_label_set(m_actions_bt, "Actions");
    evas_object_smart_callback_add(m_actions_bt, "clicked", actions_action, this);
    evas_object_size_hint_weight_set(m_actions_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(m_actions_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, m_actions_bt);
    evas_object_show(m_actions_bt);

    Evas_Object* open_bt = elm_button_add(win);
    elm_button_label_set(open_bt, "Open");
    evas_object_smart_callback_add(open_bt, "clicked", open_action, this);
    evas_object_size_hint_weight_set(open_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(open_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, open_bt);
    evas_object_show(open_bt);
}

FolderWindow::~FolderWindow()
{
    //dtor
}

void FolderWindow::close_action(void *data, Evas_Object *obj, void *event_info)
{
    Controller::getInstance()->quit();
}

void FolderWindow::open_action(void *data, Evas_Object *obj, void *event_info){
    FolderWindow* self = static_cast<FolderWindow*>(data);
    if(self->m_selected_folder != NULL){
        Controller::getInstance()->openHeaderList(self->m_selected_folder);
    }
}

void FolderWindow::select_action(void *data, Evas_Object *obj, void *event_info){
    CallbackParam* param = static_cast<CallbackParam*>(data);
    DEBUG4("FolderWindow::select_action folder: " << param->folder->getFoldername() << " selected");
    param->self->m_selected_folder = param->folder;
}


void FolderWindow::actions_action(void *data, Evas_Object *obj, void *event_info){
    FolderWindow* self = static_cast<FolderWindow*>(data);
    self->showActionsHover();
}


void FolderWindow::refresh_action(void *data, Evas_Object *obj, void *event_info){
    FolderWindow* self = static_cast<FolderWindow*>(data);
    if(self->m_selected_folder != NULL){
        Controller::getInstance()->refreshSelectedFolder(self->m_selected_folder);
    }
    self->closeActionsHover();
}

void FolderWindow::refreshall_action(void *data, Evas_Object *obj, void *event_info){
    FolderWindow* self = static_cast<FolderWindow*>(data);
    Controller::getInstance()->refreshAllFolders();
    self->closeActionsHover();
}

void FolderWindow::markread_action(void *data, Evas_Object *obj, void *event_info){
    FolderWindow* self = static_cast<FolderWindow*>(data);
    if(self->m_selected_folder != NULL){
        Controller::getInstance()->markFolderRead(self->m_selected_folder);
    }
    self->closeActionsHover();
}


void FolderWindow::updateFolderlist(std::list<Folder*> folderlist){
    m_selected_folder = NULL;
    elm_list_clear(m_folderlist);

    std::list< Folder* >::iterator it;
    for (it=folderlist.begin(); it != folderlist.end(); it++){
         Evas_Object* info = createFolderInfoWidget((*it)->getMessageCount(), (*it)->getUnreadMessageCount());

         Evas_Object* ic = elm_icon_add(m_win);
         char buf[PATH_MAX];
         std::string iconpath = ConfigManager::getInstance()->getLocation() + "share/vilari_mail/icons/folder.png";
         elm_icon_file_set(ic, iconpath.c_str(), NULL);
         elm_icon_scale_set(ic, 0, 0);
         evas_object_show(ic);

         CallbackParam* param = new CallbackParam();
         param->self = this;
         param->folder = (*it);

         elm_list_item_append(m_folderlist, (*it)->getFoldername().c_str(), ic, info, select_action, param);
         DEBUG4("FolderWindow::updateFolderlist adding folder:" << (*it)->getFoldername());
    }

    elm_list_go(m_folderlist);

}

Evas_Object* FolderWindow::createFolderInfoWidget(int total, int unread){
    Evas_Object* verbox = elm_box_add(m_win);
    evas_object_size_hint_weight_set(verbox, 1.0, 1.0);
    evas_object_size_hint_align_set(verbox, -1.0, -1.0);
    evas_object_show(verbox);

    Evas_Object* lb1 = elm_label_add(m_win);

    std::ostringstream unreadtext;
    unreadtext << "<b>" << unread << " Unread</b>";
    elm_label_label_set(lb1, unreadtext.str().c_str());
    evas_object_size_hint_weight_set(lb1, 1.0, 0.0);
    evas_object_size_hint_align_set(lb1, 1.0, 0.5);
    elm_box_pack_end(verbox, lb1);
    evas_object_show(lb1);

    Evas_Object* lb2 = elm_label_add(m_win);
    std::ostringstream totaltext;
    totaltext << total << " Total";
    elm_label_label_set(lb2, totaltext.str().c_str());
    evas_object_size_hint_weight_set(lb2, 1.0, 0.0);
    evas_object_size_hint_align_set(lb2, 1.0, 0.5);
    elm_box_pack_end(verbox, lb2);
    evas_object_show(lb2);

    return verbox;
}


void FolderWindow::showActionsHover(){

    m_hover = elm_hover_add(m_win);
    evas_object_size_hint_weight_set(m_hover, 1.0, 1.0);
    evas_object_size_hint_align_set(m_hover, 1.0, 1.0);
    evas_object_show(m_hover);

    Evas_Object* bx = elm_box_add(m_win);
    evas_object_size_hint_weight_set(bx, 1.0, 0.0);
    evas_object_size_hint_align_set(bx, 1.0, 1.0);
    evas_object_show(bx);

    elm_hover_content_set(m_hover, "top", bx);
    elm_hover_parent_set(m_hover, m_win);
    elm_hover_target_set(m_hover, m_actions_bt);

    if(m_selected_folder  != NULL){
        Evas_Object* read_bt = elm_button_add(m_win);
        elm_button_label_set(read_bt, "mark folder read");
        evas_object_smart_callback_add(read_bt, "clicked", markread_action, this);
        evas_object_size_hint_weight_set(read_bt, 1.0, 0.0);
        evas_object_size_hint_align_set(read_bt, -1.0, -1.0);
        elm_box_pack_end(bx, read_bt);
        evas_object_show(read_bt);

        Evas_Object* refresh_bt = elm_button_add(m_win);
        elm_button_label_set(refresh_bt, "refresh folder");
        evas_object_smart_callback_add(refresh_bt, "clicked", refresh_action, this);
        evas_object_size_hint_weight_set(refresh_bt, 1.0, 0.0);
        evas_object_size_hint_align_set(refresh_bt, -1.0, -1.0);
        elm_box_pack_end(bx, refresh_bt);
        evas_object_show(refresh_bt);

    }

    Evas_Object* refresh_all_bt = elm_button_add(m_win);
    elm_button_label_set(refresh_all_bt, "refresh all folders");
    evas_object_smart_callback_add(refresh_all_bt, "clicked", refreshall_action, this);
    evas_object_size_hint_weight_set(refresh_all_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(refresh_all_bt, -1.0, -1.0);
    elm_box_pack_end(bx, refresh_all_bt);
    evas_object_show(refresh_all_bt);


}

void FolderWindow::closeActionsHover(){
    evas_object_hide(m_hover);
    evas_object_del(m_hover);
}

