/*
 *      ConfigManager.h
 *
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H


#include <commons.h>
#include <Account.h>
#include <yaml.h>
#include <Util.h>

class ConfigManager
{
    public:
        static ConfigManager* getInstance();
        static void destroy();

        void init(std::string executablepath);


        std::string getLocation();

        bool isDebug();
        void setDebug(bool isdebug);

        bool isMemDebug();
        void setMemDebug(bool isdebug);

        int getMessagesPerPage();

        std::list<Account*> getAccountList();

    protected:

    private:
        ConfigManager();
        ConfigManager(const ConfigManager& cm);
        virtual ~ConfigManager();

        void setLocation(std::string executablepath);
        void prepareConfiguration();

        void readConfigurationFiles();

        void readBaseConfiguration();
        void readImapAccounts();

        void handleYamlEvent(yaml_event_t* event);


        static ConfigManager* m_instance;

        std::string m_location;

        int m_messagesperpage;
        std::list<Account*> m_acc_list;
        std::string m_homedir;
        bool m_isdebug;
        bool m_ismemdebug;
};

#endif // CONFIGMANAGER_H
