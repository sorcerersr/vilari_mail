/*
 *      Controller.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H


#include <commons.h>

#include <Account.h>
#include <IMAP.h>

// forward declaration for cross reference
class MainWindow;
#include <MainWindow.h>


class Controller
{
    public:
        static Controller* getInstance();
        static void destroy();

        void connect(Account* account);
        void goBack();
        void openHeaderList(Folder* folder);

        void updateHeaderList(Folder* folder, int index);
        void refreshHeaderListGUI();


        void refreshSelectedFolder(Folder* folder);
        void refreshAllFolders();


        void showMessage(Header* header);

        void toggleReadFlag(Header* header);
        void markFolderRead(Folder* folder);

        void runApp();
        void quit();

    protected:
    private:
         Controller();
         Controller(const Controller& imap);
         virtual ~Controller();
         static Controller* m_instance;

         void setFolderList(std::list< Folder* > folderlist);
         void setHeaderList(std::list< Header* > headerlist);

         std::list< Folder* > m_currentfolderlist;

         std::list< Header* > m_currentheaderlist;
         Folder* m_current_folder;
         int m_current_index_from;
         int m_current_index_to;

         Message* m_currentmessage;

         MainWindow* m_mainwin;

};

#endif // CONTROLLER_H
