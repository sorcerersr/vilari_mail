/*
 *      Header.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HEADER_H
#define HEADER_H

#include <commons.h>
#include <Address.h>
#include <Folder.h>

class Header
{
    public:
        Header(Folder* folder, uint32_t messagenum);
        virtual ~Header();

        Folder* getFolder();
        uint32_t getMessageNum();

        void setReadFlag(bool readflag);
        bool getReadFlag();

        void setDate(std::string date);
        std::string getDate();

        void setSubject(std::string subject);
        std::string getSubject();

        void addFrom(Address* fromaddress);
        void addReplyTo(Address* replytoaddress);
        void addTo(Address* toaddress);
        void addCC(Address* ccaddress);

        std::list<Address*> getFromList();
        std::list<Address*> getReplyToList();
        std::list<Address*> getToList();
        std::list<Address*> getCCList();



        std::string toString();


    protected:
    private:
       bool m_readflag;
       Folder* m_folder;
       uint32_t m_messagenum;
       std::string m_date;
       std::string m_subject;
       std::list<Address*> m_fromlist;
       std::list<Address*> m_replytolist;
       std::list<Address*> m_tolist;
       std::list<Address*> m_cclist;



};

#endif // HEADER_H
