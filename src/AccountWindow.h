/*
 *      AccountWindow.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ACCOUNTWINDOW_H
#define ACCOUNTWINDOW_H

#include <commons.h>
#include <Content.h>
#include <ConfigManager.h>
#include <Controller.h>
#include <Account.h>


class AccountWindow : public Content
{



    public:
        AccountWindow(Evas_Object* win);
        virtual ~AccountWindow();
    protected:
        void blub();
    private:
        struct CallbackParam {
            AccountWindow* self;
            Account* account;
        };

        Evas_Object* m_hoversel;

        Account* m_selected_account;
        void fillAccountHoversel();

        static void hoverselection_action(void *data, Evas_Object *obj, void *event_info);

        static void connect_action(void *data, Evas_Object *obj, void *event_info);

        static void close_action(void *data, Evas_Object *obj, void *event_info);
};

#endif // ACCOUNTWINDOW_H
