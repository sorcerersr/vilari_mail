/*
 *      MessageListWindow.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MESSAGELISTWINDOW_H
#define MESSAGELISTWINDOW_H

#include <commons.h>
#include <Content.h>
#include <Controller.h>
#include <Header.h>

class MessageListWindow : public Content
{
    public:
        MessageListWindow(Evas_Object* win);
        virtual ~MessageListWindow();

        void updateMessagelist(Folder* folder,
                               std::list<Header*> headerlist,
                               int index_from,
                               int index_to,
                               int folder_unread,
                               int folder_total
                               );
    protected:
    private:
        struct CallbackParam {
            MessageListWindow* self;
            Header* header;
        };


        static void back_action(void *data, Evas_Object *obj, void *event_info);
        static void open_action(void *data, Evas_Object *obj, void *event_info);
        static void select_action(void *data, Evas_Object *obj, void *event_info);
        static void actions_action(void *data, Evas_Object *obj, void *event_info);

        static void readbutton_action(void *data, Evas_Object *obj, void *event_info);

        static void page_forward_action(void *data, Evas_Object *obj, void *event_info);
        static void page_backward_action(void *data, Evas_Object *obj, void *event_info);

        void updateInfoFrame(int index_from, int index_to, int folder_unread, int folder_total);

        void clearList();

        void showActionsHover();

        int m_index_from;
        int m_index_to;


        Evas_Object* m_index_label;
        Evas_Object* m_folder_frame;

        Evas_Object* m_hover;
        Evas_Object* m_action_bt;

        Folder* m_folder;
        Header* m_current_selected_header;
        Evas_Object* m_messagelist;
};

#endif // MESSAGELISTWINDOW_H
