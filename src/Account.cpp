/*
 *      Account.cpp
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Account.h"

Account::Account()
{
    m_default = false;
    m_ssl = false;
    m_port = 110;
}

Account::~Account()
{
    //dtor
}



void Account::setName(std::string name){
    m_name = name;
}

std::string Account::getName(){
    return m_name;
}

void Account::setType(std::string type){
    m_type = type;
}

std::string Account::getType(){
    return m_type;
}

void Account::setServer(std::string server){
    m_server = server;
}

std::string Account::getServer(){
    return m_server;
}

void Account::setPort(uint port){
    m_port = port;
}

uint Account::getPort(){
    return m_port;
}

void Account::setUsername(std::string username){
    m_username = username;
}

std::string Account::getUsername(){
    return m_username;
}

void Account::setPassword(std::string password){
    m_password = password;
}

std::string Account::getPassword(){
    return m_password;
}

void Account::setSSL(bool usessl){
    m_ssl = usessl;
}

bool Account::useSSL(){
    return m_ssl;
}

void Account::setDefault(bool isdefault){
    m_default = isdefault;
}

bool Account::isDefault(){
    return m_default;
}


std::string Account::toString(){
    std::ostringstream result;
    result << "Account: " << m_name << std::endl;
    result << "         Server: " << m_server << std::endl;
    result << "         Port: " << m_port << std::endl;
    result << "         Type: " << m_type << std::endl;
    result << "         User: " << m_username << std::endl;
    result << "         Pass: " << m_password << std::endl;
    result << "         SSL:  " << m_ssl << std::endl;
    result << "         default: " << m_default << std::endl;
    return result.str();
}
