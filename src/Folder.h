/*
 *      Folder.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FOLDER_H
#define FOLDER_H

#include "commons.h"
#include "ConfigManager.h"

class Folder
{
    public:
        Folder(std::string foldername);
        virtual ~Folder();

        const std::string getFoldername();

        void setMessageCount(int messagecount);
        const int getMessageCount();

        void setUnreadMessageCount(int unreadmessagecount);
        const int getUnreadMessageCount();

    protected:
    private:
        std::string m_foldername;
        std::string m_displayname;
        int m_messagecount;
        int m_messagecount_unread;
};

#endif // FOLDER_H
