/*
 *      Util.cpp
 *
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Util.h"

Util::Util(){/*ctor*/}

Util::~Util(){/*dtor*/}


std::string Util::str_replace(std::string source, std::string search, std::string replace){
    std::string::size_type pos = source.find(search, 0);
    int intLengthSearch = search.length();

    while(std::string::npos != pos )
    {
            source.replace(pos, intLengthSearch, replace);
            pos = source.find(search, 0);
    }
    return source;
}


bool Util::checkDirOrFileExists(std::string dir){

    struct stat statinfo;
	int result = stat(dir.c_str(), &statinfo);
	if (result < 0) {
	    // this is not absolutely correct result < 0 doesn't
        // neccessary means the file/dir does not exist
        // ( errno needs to be checked ), but for the moment
        // it should be enough
        return false;
	}

	return true;
}


bool Util::copyFile(std::string sourcefile, std::string destfile){

    std::ifstream ifs;
    ifs.open(sourcefile.c_str(), std::ios::in);

    std::ofstream ofs;
    ofs.open(destfile.c_str(), std::ios::out);

    ofs << ifs.rdbuf();

    ofs.close();
    ifs.close();

}
