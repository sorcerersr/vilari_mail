/*
 *      Folder.cpp
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Folder.h"

Folder::Folder(std::string foldername)
{
    MEMDEBUG("### CTOR Folder: " << foldername );
    m_foldername = foldername;

    // initialize both message counters with -1
    m_messagecount = -1;
    m_messagecount_unread = -1;

    DEBUG4("Folder created: " << m_foldername );

}

Folder::~Folder()
{
    MEMDEBUG("### DTOR Folder " << m_foldername);
    //dtor
}



const std::string Folder::getFoldername(){
    return m_foldername;
}

void Folder::setMessageCount(int messagecount){
    m_messagecount = messagecount;
}

const int Folder::getMessageCount(){
    return m_messagecount;
}


void Folder::setUnreadMessageCount(int unreadmessagecount){
    m_messagecount_unread = unreadmessagecount;
}

const int Folder::getUnreadMessageCount(){
    return m_messagecount_unread;
}
