/*
 *      FolderWindow.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FOLDERWINDOW_H
#define FOLDERWINDOW_H

#include <Content.h>
#include <Folder.h>
#include <Controller.h>


class FolderWindow : public Content
{
    public:
        FolderWindow(Evas_Object* win);
        virtual ~FolderWindow();

        void updateFolderlist(std::list<Folder*> folderlist);

    protected:
    private:
        struct CallbackParam {
            FolderWindow* self;
            Folder* folder;
        };

        static void close_action(void *data, Evas_Object *obj, void *event_info);
        static void open_action(void *data, Evas_Object *obj, void *event_info);
        static void select_action(void *data, Evas_Object *obj, void *event_info);
        static void actions_action(void *data, Evas_Object *obj, void *event_info);

        static void refresh_action(void *data, Evas_Object *obj, void *event_info);
        static void refreshall_action(void *data, Evas_Object *obj, void *event_info);
        static void markread_action(void *data, Evas_Object *obj, void *event_info);

        void showActionsHover();
        void closeActionsHover();

        Evas_Object* createFolderInfoWidget(int total, int unread);

        Evas_Object* m_hover;
        Evas_Object* m_actions_bt;

        Folder* m_selected_folder;
        Evas_Object* m_folderlist;
};

#endif // FOLDERWINDOW_H
