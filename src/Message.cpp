/*
 *      Message.cpp
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Message.h"

Message::Message(Header* header)
{
    m_header = header;
}

Message::~Message()
{
    //dtor
}


Header* Message::getHeader(){
    return m_header;
}



void Message::setText(std::string text){
    m_text = text;
}

std::string Message::getText(){
    return m_text;
}


std::string Message::toString(){
    std::ostringstream result;
    result << m_header->toString() << std::endl;
    result << m_text << std::endl;
    return result.str();

}
