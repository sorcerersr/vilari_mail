/*
 *      Header.cpp
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Header.h"

Header::Header(Folder* folder, uint32_t messagenum)
{
    m_folder = folder;
    m_messagenum = messagenum;
    m_readflag = false;
}

Header::~Header()
{
    std::list< Address* >::iterator it;
    for (it=m_fromlist.begin(); it != m_fromlist.end(); it++){
        delete (*it);
    }

    for (it=m_replytolist.begin(); it != m_replytolist.end(); it++){
        delete (*it);
    }

    for (it=m_tolist.begin(); it != m_tolist.end(); it++){
        delete (*it);
    }

    for (it=m_cclist.begin(); it != m_cclist.end(); it++){
        delete (*it);
    }

}


Folder* Header::getFolder(){
        return m_folder;
}

uint32_t Header::getMessageNum(){
    return m_messagenum;
}

void Header::setReadFlag(bool readflag){
    m_readflag = readflag;
}


bool Header::getReadFlag(){
    return m_readflag;
}

void Header::setDate(std::string date){
    m_date = date;
}

std::string Header::getDate(){
    return m_date;
}

void Header::setSubject(std::string subject){
    m_subject = subject;
}

std::string Header::getSubject(){
    return m_subject;
}

void Header::addFrom(Address* fromaddress){
    m_fromlist.push_back(fromaddress);
}

void Header::addReplyTo(Address* replytoaddress){
    m_replytolist.push_back(replytoaddress);
}

void Header::addTo(Address* toaddress){
    m_tolist.push_back(toaddress);
}

void Header::addCC(Address* ccaddress){
    m_cclist.push_back(ccaddress);
}


std::list<Address*> Header::getFromList(){
    return m_fromlist;
}

std::list<Address*> Header::getReplyToList(){
    return m_replytolist;
}

std::list<Address*> Header::getToList(){
    return m_tolist;
}

std::list<Address*> Header::getCCList(){
    return m_cclist;
}


std::string Header::toString(){
    std::ostringstream result;
    result << "  # Header for Message with NUM: " << m_messagenum << " in folder " << m_folder->getFoldername() << std::endl;
    result << "            Subject:     " << m_subject << std::endl;
    result << "            From:        ";
    std::list< Address* >::iterator it;
    for (it=m_fromlist.begin(); it != m_fromlist.end(); it++){
        result << (*it)->getDisplayName() << " <" << (*it)->getAddress() << ">;";
    }
    result << std::endl;
    result << "            Date:        " << m_date << std::endl;
    result << "            To:          ";


    for (it=m_tolist.begin(); it != m_tolist.end(); it++){
        result << (*it)->getDisplayName() << " <" << (*it)->getAddress() << ">;";
    }
    result << std::endl;
    return result.str();

}
