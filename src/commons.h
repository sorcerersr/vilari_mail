/*
 *      commons.h
 *
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef COMMONS_H
#define COMMONS_H

#include <stdlib.h>
#include <stdint.h>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <list>
#include <cassert>


//needed for libetpan-mailimap
#define DEFAULT_CHARSET "utf-8"


//DEBUG Makro definition. Asks Configmanager if debug-output is enabled first and then output to stdout
#define INFO(output) std::cout << "INFO: " << output << std::endl

//define some DEBUG output macros, they differ only in the level of intendation
#define DEBUG(output) if(ConfigManager::getInstance()->isDebug() ) std::cout << "DEBUG: " << output << std::endl
#define DEBUG2(output) if(ConfigManager::getInstance()->isDebug() ) std::cout << "DEBUG:\t " << output << std::endl
#define DEBUG3(output) if(ConfigManager::getInstance()->isDebug() ) std::cout << "DEBUG:\t\t " << output << std::endl
#define DEBUG4(output) if(ConfigManager::getInstance()->isDebug() ) std::cout << "DEBUG:\t\t\t " << output << std::endl

#define MEMDEBUG(output) if(ConfigManager::getInstance()->isMemDebug() ) std::cout << "MEMDEBUG: " << output << std::endl

#define ERROR(output) std::cerr << "ERROR: " << output << std::endl


#endif // COMMONS_H
