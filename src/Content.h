/*
 *      Content.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTENT_H
#define CONTENT_H

#include <commons.h>
#include <Elementary.h>

class Content
{
    public:
        Content(Evas_Object* win);
        virtual ~Content();

        Evas_Object* getElmContent();

    protected:
        Evas_Object* m_content_box;
        Evas_Object* m_win;
    private:




};

#endif // CONTENT_H
