/*
 *      Util.h
 *
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <iostream>
#include <fstream>
#include <sys/stat.h>


class Util
{
    public:

        static std::string str_replace(std::string source, std::string search, std::string replace);

        static bool checkDirOrFileExists(std::string dir);

        static bool copyFile(std::string sourcefile, std::string destfile);

    protected:
    private:
        // this class just offers static helper
        // methods so constructor is private
        Util();
        virtual ~Util();
};

#endif // UTIL_H
