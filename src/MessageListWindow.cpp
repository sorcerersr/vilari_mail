/*
 *      MessageListWindow.cpp
 *
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "MessageListWindow.h"

MessageListWindow::MessageListWindow(Evas_Object* win)  : Content(win)
{
    m_current_selected_header = NULL;

    Evas_Object* topbx = elm_box_add(win);
    elm_box_horizontal_set(topbx, true);
    evas_object_size_hint_weight_set(topbx, 1.0, 0.0);
    evas_object_size_hint_align_set(topbx, -1.0, -1.0);
    elm_box_pack_end(m_content_box, topbx);
    evas_object_show(topbx);

    Evas_Object* back_bt = elm_button_add(win);
    elm_button_label_set(back_bt, "<");
    evas_object_smart_callback_add(back_bt, "clicked", page_backward_action, this);
    evas_object_size_hint_weight_set(back_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(back_bt, -1.0, -1.0);
    elm_box_pack_end(topbx, back_bt);
    evas_object_show(back_bt);


    m_folder_frame = elm_frame_add(win);
    evas_object_size_hint_weight_set(m_folder_frame, 1.0, 0.0);
    evas_object_size_hint_align_set(m_folder_frame, -1.0, -1.0);

    elm_box_pack_end(topbx, m_folder_frame);

    evas_object_show(m_folder_frame);

    m_index_label = elm_label_add(win);

    elm_frame_content_set(m_folder_frame, m_index_label);
    evas_object_show(m_index_label);


    Evas_Object* forward_bt = elm_button_add(win);
    elm_button_label_set(forward_bt, ">");
    evas_object_smart_callback_add(forward_bt, "clicked", page_forward_action, this);
    evas_object_size_hint_weight_set(forward_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(forward_bt, -1.0, -1.0);
    elm_box_pack_end(topbx, forward_bt);
    evas_object_show(forward_bt);



    Evas_Object* verbox = elm_box_add(win);
    evas_object_size_hint_weight_set(verbox, 1.0, 1.0);
    evas_object_size_hint_align_set(verbox, -1.0, -1.0);
    elm_box_pack_end(m_content_box, verbox);
    evas_object_show(verbox);


    m_messagelist = elm_list_add(win);
    elm_box_pack_end(verbox, m_messagelist);
    evas_object_size_hint_weight_set(m_messagelist, 1.0, 1.0);
    evas_object_size_hint_align_set(m_messagelist, -1.0, -1.0);

    elm_scroller_bounce_set(m_messagelist ,false, false);

    elm_list_go(m_messagelist);
    evas_object_show(m_messagelist);

    Evas_Object* horbx = elm_box_add(win);
    elm_box_horizontal_set(horbx, true);
    evas_object_size_hint_weight_set(horbx, 1.0, 0.0);
    evas_object_size_hint_align_set(horbx, -1.0, -1.0);
    elm_box_pack_end(m_content_box, horbx);
    evas_object_show(horbx);


    Evas_Object* close_bt = elm_button_add(win);
    elm_button_label_set(close_bt, "Back");
    evas_object_smart_callback_add(close_bt, "clicked", back_action, NULL);
    evas_object_size_hint_weight_set(close_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(close_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, close_bt);
    evas_object_show(close_bt);


    m_action_bt = elm_button_add(win);
    elm_button_label_set(m_action_bt, "Actions");
    evas_object_smart_callback_add(m_action_bt, "clicked", actions_action, this);
    evas_object_size_hint_weight_set(m_action_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(m_action_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, m_action_bt);
    evas_object_show(m_action_bt);

    Evas_Object* open_bt = elm_button_add(win);
    elm_button_label_set(open_bt, "Open");
    evas_object_smart_callback_add(open_bt, "clicked", open_action, this);
    evas_object_size_hint_weight_set(open_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(open_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, open_bt);
    evas_object_show(open_bt);
}

MessageListWindow::~MessageListWindow()
{
    //dtor
}


void MessageListWindow::back_action(void *data, Evas_Object *obj, void *event_info){
    Controller::getInstance()->goBack();
}


void MessageListWindow::select_action(void *data, Evas_Object *obj, void *event_info){
    CallbackParam* param = static_cast<CallbackParam*>(data);
    DEBUG4("MessageListWindow::select_action");
    param->self->m_current_selected_header = param->header;
}

void MessageListWindow::open_action(void *data, Evas_Object *obj, void *event_info){
    MessageListWindow* self = static_cast<MessageListWindow*>(data);
    if(self->m_current_selected_header != NULL){
        Controller::getInstance()->showMessage(self->m_current_selected_header);
    }
}


void MessageListWindow::page_forward_action(void *data, Evas_Object *obj, void *event_info){
    MessageListWindow* self = static_cast<MessageListWindow*>(data);
    int messagesperpage = ConfigManager::getInstance()->getMessagesPerPage();
    Controller::getInstance()->updateHeaderList(self->m_folder, self->m_index_from + messagesperpage);
}

void MessageListWindow::page_backward_action(void *data, Evas_Object *obj, void *event_info){
    MessageListWindow* self = static_cast<MessageListWindow*>(data);
    int messagesperpage = ConfigManager::getInstance()->getMessagesPerPage();
    Controller::getInstance()->updateHeaderList(self->m_folder, self->m_index_from - messagesperpage);
}


void MessageListWindow::actions_action(void *data, Evas_Object *obj, void *event_info){
    MessageListWindow* self = static_cast<MessageListWindow*>(data);
    if(self->m_current_selected_header != NULL){
        self->showActionsHover();
    }
}


void MessageListWindow::readbutton_action(void *data, Evas_Object *obj, void *event_info){
    MessageListWindow* self = static_cast<MessageListWindow*>(data);
    Controller::getInstance()->toggleReadFlag(self->m_current_selected_header);
    evas_object_hide(self->m_hover);
    evas_object_del(self->m_hover);
}




void MessageListWindow::updateInfoFrame(int index_from, int index_to, int folder_unread, int folder_total){
    std::ostringstream folderinfo;
    folderinfo << folder_unread << " / " << folder_total;
    elm_frame_label_set(m_folder_frame, folderinfo.str().c_str());

    std::ostringstream pageinfo;
    pageinfo << index_from << " / " << index_to;
    elm_label_label_set(m_index_label, pageinfo.str().c_str());


}



void MessageListWindow::clearList(){

    elm_list_clear(m_messagelist);
}

void MessageListWindow::updateMessagelist(Folder* folder,
                                          std::list<Header*> headerlist,
                                          int index_from,
                                          int index_to,
                                          int folder_unread,
                                          int folder_total){

    m_current_selected_header = NULL;
    m_folder = folder;
    updateInfoFrame(index_from, index_to, folder_unread, folder_total);
    m_index_from = index_from;
    m_index_to = index_to;

    clearList();

    std::string readiconpath = ConfigManager::getInstance()->getLocation() + "share/vilari_mail/icons/mail_normal.png";
    std::string unreadiconpath = ConfigManager::getInstance()->getLocation() + "share/vilari_mail/icons/mail_new.png";

    std::list< Header* >::iterator it;
    for (it=headerlist.begin(); it != headerlist.end(); it++){
         //Evas_Object* info = createFolderInfoWidget((*it)->getMessageCount(), (*it)->getUnreadMessageCount());

         Evas_Object* ic = elm_icon_add(m_win);
         if ( (*it)->getReadFlag() == true ){
            elm_icon_file_set(ic, readiconpath.c_str(), NULL);
         } else {

            elm_icon_file_set(ic, unreadiconpath.c_str(), NULL);
         }

         elm_icon_scale_set(ic, 0, 0);
         evas_object_show(ic);

         CallbackParam* param = new CallbackParam();
         param->self = this;
         param->header = (*it);

         std::string subject = (*it)->getSubject();
         if (subject.length() > 30){
            subject = subject.substr(0,30);
         }

         //elm_list_item_append(m_folderlist, (*it)->getFoldername().c_str(), ic, info, select_action, param);
         elm_list_item_append(m_messagelist, subject.c_str(), ic, NULL, select_action, param);
         DEBUG4("MessageListWindow::updateMessagelist adding header for Message with NUM:" << (*it)->getMessageNum());
    }

    elm_list_go(m_messagelist);

}



void MessageListWindow::showActionsHover(){

    m_hover = elm_hover_add(m_win);
    evas_object_size_hint_weight_set(m_hover, 1.0, 1.0);
    evas_object_size_hint_align_set(m_hover, 1.0, 1.0);
    evas_object_show(m_hover);

    Evas_Object* bx = elm_box_add(m_win);
    evas_object_size_hint_weight_set(bx, 1.0, 0.0);
    evas_object_size_hint_align_set(bx, 1.0, 1.0);
    evas_object_show(bx);

    elm_hover_content_set(m_hover, "top", bx);
    elm_hover_parent_set(m_hover, m_win);
    elm_hover_target_set(m_hover, m_action_bt);

    Evas_Object* move_bt = elm_button_add(m_win);
    elm_button_label_set(move_bt, "move message");
    //evas_object_smart_callback_add(move_bt, "clicked", close_action, NULL);
    evas_object_size_hint_weight_set(move_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(move_bt, -1.0, -1.0);
    elm_box_pack_end(bx, move_bt);
    evas_object_show(move_bt);

    Evas_Object* read_bt = elm_button_add(m_win);

    if(m_current_selected_header->getReadFlag()){
        elm_button_label_set(read_bt, "mark as unread");
    } else {
        elm_button_label_set(read_bt, "mark as read");
    }
    evas_object_smart_callback_add(read_bt, "clicked", readbutton_action, this);
    evas_object_size_hint_weight_set(read_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(read_bt, -1.0, -1.0);
    elm_box_pack_end(bx, read_bt);
    evas_object_show(read_bt);


}



