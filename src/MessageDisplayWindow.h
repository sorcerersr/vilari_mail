/*
 *      MessageDisplayWindow.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MESSAGEDISPLAYWINDOW_H
#define MESSAGEDISPLAYWINDOW_H

#include <commons.h>
#include <Content.h>
#include <Controller.h>
#include <Message.h>



class MessageDisplayWindow : public Content
{
    public:
        MessageDisplayWindow(Evas_Object* win);
        virtual ~MessageDisplayWindow();

        void updateMessageDisplay(Message* message);

    protected:
    private:

        static void back_action(void *data, Evas_Object *obj, void *event_info);
        static void actions_action(void *data, Evas_Object *obj, void *event_info);

        static void readbutton_action(void *data, Evas_Object *obj, void *event_info);

        void showActionsHover();

        Evas_Object* m_action_bt;
        Evas_Object* m_hover;
        Evas_Object* m_entry;
        Message* m_message;
};

#endif // MESSAGEDISPLAYWINDOW_H
