/*
 *      MainWindow.cpp
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "MainWindow.h"

MainWindow::MainWindow()
{

    MEMDEBUG("MAINWINDOW created");

    m_window = elm_win_add(NULL, "vilari_mail", ELM_WIN_BASIC);
    elm_win_title_set(m_window, "Vilari Mail");
    evas_object_resize(m_window, 225, 255);
    evas_object_smart_callback_add(m_window, "delete-request", win_del, NULL);


    Evas_Object* bg = elm_bg_add(m_window);
    evas_object_size_hint_weight_set(bg, 1.0, 1.0);
    elm_win_resize_object_add(m_window, bg);
    evas_object_show(bg);

    initWindows();

    displayAccWindow();

    evas_object_show(m_window);

}

MainWindow::~MainWindow()
{

   delete m_acc_win;
   elm_shutdown();

}


void MainWindow::initWindows(){
    m_acc_win = new AccountWindow(m_window);
    m_folder_win = new FolderWindow(m_window);
    m_msg_lst_win = new MessageListWindow(m_window);
    m_msg_dsp_win = new MessageDisplayWindow(m_window);
}


void MainWindow::displayContent(Content* content){
    DEBUG4("MainWindow::displayContent - Add the new content to the window!");
    elm_win_resize_object_add(m_window, content->getElmContent());
    DEBUG4("MainWindow::displayContent - Show the new content");
    evas_object_show(content->getElmContent());
}

void MainWindow::removeContent(Content* content){
    DEBUG4("MainWindow::removeContent - Hide previous content");
    evas_object_hide(content->getElmContent());
    DEBUG4("MainWindow::removeContent - remove previous content");
    elm_win_resize_object_del(m_window, content->getElmContent());
}

void MainWindow::switchContent(Content* newcontent){
    DEBUG4("MainWindow::switchContent called");


    if( !m_contents.empty() ){
        removeContent(m_contents.back());
    }


    m_contents.push_back(newcontent);
    displayContent(newcontent);

}


Content* MainWindow::getLastContent(){
    return m_contents.back();
}


void MainWindow::win_del(void *data, Evas_Object *obj, void *event_info)
{
    Controller::getInstance()->quit();
}

void MainWindow::displayLastContentWindow(){
    DEBUG4("MainWindow::displayLastContentWindow called");

    removeContent(m_contents.back());
    m_contents.pop_back();

    displayContent(m_contents.back());
}

void MainWindow::displayAccWindow(){
    DEBUG4("MainWindow::displayFolderWindow called");
    switchContent(m_acc_win);
}


void MainWindow::displayFolderWindow(){
    DEBUG4("MainWindow::displayFolderWindow called");
    switchContent(m_folder_win);
}

void MainWindow::displayMessageListWindow(){
    DEBUG4("MainWindow::displayMessageListWindow called");
    switchContent(m_msg_lst_win);
}

void MainWindow::displayMessageDisplayWindow(){
    DEBUG4("MainWindow::displayMessageDisplayWindow called");
    switchContent(m_msg_dsp_win);
}


void MainWindow::updateFolderWindow(std::list< Folder* > folderlist){
    DEBUG4("MainWindow::updateFolderWindow called");
    m_folder_win->updateFolderlist(folderlist);
}

void MainWindow::updateMessageListWindow(Folder* folder,
                                         std::list<Header*> headerlist,
                                         int index_from,
                                         int index_to,
                                         int folder_unread,
                                         int folder_total){
    DEBUG4("MainWindow::updateMessageListWindow called");
    m_msg_lst_win->updateMessagelist(folder, headerlist, index_from, index_to, folder_unread, folder_total);
}


void MainWindow::updateMessageDisplayWindow(Message* message){
    DEBUG4("MainWindow::updateMessageDisplayWindow called");
    m_msg_dsp_win->updateMessageDisplay(message);
}
