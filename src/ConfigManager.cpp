/*
 *      ConfigManager.cpp
 *
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "ConfigManager.h"




ConfigManager* ConfigManager::m_instance = NULL;

ConfigManager::ConfigManager()
{
    //debug output is off by default
    m_isdebug = false;
    m_ismemdebug = false;

    // get path to home dir
    m_homedir = getenv("HOME");

    // set default for messages per page variable
    m_messagesperpage = 10;

}

ConfigManager::~ConfigManager()
{
    //dtor
}

ConfigManager::ConfigManager(const ConfigManager& cm){
    //copy ctor
}

ConfigManager* ConfigManager::getInstance(){
    if( !m_instance ){
      m_instance = new ConfigManager();
    }
    return m_instance;
}

void ConfigManager::destroy(){
    if( m_instance ){
      delete m_instance;
      m_instance = 0;
    }
}


bool ConfigManager::isDebug(){
    return m_isdebug;
}

void ConfigManager::setDebug(bool isdebug){
    m_isdebug = isdebug;
}


bool ConfigManager::isMemDebug(){
    return m_ismemdebug;
}

void ConfigManager::setMemDebug(bool ismemdebug){
    m_ismemdebug = ismemdebug;
}


/** @brief Initializes the ConfigManager and reads the configuration files
*
*  Initializes the Configmanager and reads the configuration files
*/
void ConfigManager::init(std::string executablepath){

    setLocation(executablepath);

    prepareConfiguration();

    readConfigurationFiles();
}


/** @brief checks config and/or copy the default configuration to the home directory
*
* This method checks for the existence of the .vilari_mail directory in the
* home directory of the user running the application. Also checks if the needed
* configuration files are stored in this folder. If the directory does not exist
* it is created and the default configuration files copied into it. If one or
* more configuration file is missing the default configuration is also created
* (copied from /<installlocation>/share//vilari_mail/default_configs)
*
*/
void ConfigManager::prepareConfiguration(){
    std::string configdir = m_homedir + "/.vilari_mail";
    if( Util::checkDirOrFileExists(configdir) ){
        if( !Util::checkDirOrFileExists(configdir+"/config.yml") ){
            Util::copyFile(m_location+"share/vilari_mail/default_configs/config.yml",
                           configdir+"/config.yml");
        }

        if( !Util::checkDirOrFileExists(configdir+"/imapaccounts.yml") ){
            Util::copyFile(m_location+"share/vilari_mail/default_configs/imapaccounts.yml",
                       configdir+"/imapaccounts.yml");
        }

    } else {
        //create directory
        mkdir(configdir.c_str(), 0744);
        // copy default configs to user directory
        Util::copyFile(m_location+"share/vilari_mail/default_configs/config.yml",
                       configdir+"/config.yml");

        Util::copyFile(m_location+"share/vilari_mail/default_configs/imapaccounts.yml",
                       configdir+"/imapaccounts.yml");

    }

}

/** @brief sets the programm location based on the executable path.
  *
  * Uses the executable path (argv[0]) to determine the location  of
  * the programm. (for example used to find the share dir and the icons within)
  */
void ConfigManager::setLocation(std::string executablepath)
{
     int pos = executablepath.rfind("bin/");
     if(pos == 0){
         m_location = "./";
     } else if(pos == -1){
         //bin/ not found, must be inside bin dir!
         m_location = "../";
     } else {
         m_location = executablepath.substr(0, pos);
     }
}


std::string ConfigManager::getLocation(){
    return m_location;
}



std::list<Account*> ConfigManager::getAccountList(){
    return m_acc_list;
}


int ConfigManager::getMessagesPerPage(){
    return m_messagesperpage;
}


void ConfigManager::readImapAccounts(){
    std::string accountsfile = m_homedir + "/.vilari_mail/imapaccounts.yml";

    int doc_loop_done = 0;
    int acc_loop_done = 0;
    int data_loop_done = 0;

    int error = 0;

    yaml_parser_t parser;
    yaml_event_t event;

    FILE* file = fopen(accountsfile.c_str(), "rb");
    if(file != NULL){
        if(yaml_parser_initialize(&parser)){
            yaml_parser_set_input_file(&parser, file);

            while (!doc_loop_done)
	        {
	            if (!yaml_parser_parse(&parser, &event)) {
	                error = 1;
	                break;
	            }

                if(event.type == YAML_MAPPING_START_EVENT){

                    while(!acc_loop_done)
                    {
                        Account* account;
                        if (!yaml_parser_parse(&parser, &event)) {
                            error = 1;
                            break;
                        }


                        if(event.type == YAML_MAPPING_START_EVENT){


                            while(!data_loop_done)
                            {

                                std::string tag;


                                if (!yaml_parser_parse(&parser, &event)) {
                                    error = 1;
                                    break;
                                }

                                if(event.type == YAML_MAPPING_END_EVENT){
                                    data_loop_done = true;
                                    break;
                                }

                                if(event.type == YAML_SCALAR_EVENT){
                                    //INFO("ConfigManager::readConfigurationFiles:  DATALOOP TAG " << event.data.scalar.value);
                                    tag = (char*)event.data.scalar.value;
                                }
                                yaml_event_delete(&event);
                                if (!yaml_parser_parse(&parser, &event)) {
                                    error = 1;
                                    break;
                                }

                                if(event.type == YAML_SCALAR_EVENT){
                                    //INFO("ConfigManager::readConfigurationFiles:  DATALOOP VALUE " << event.data.scalar.value);
                                    if (tag == "type"){
                                        account->setType((char*)event.data.scalar.value);
                                    } else if(tag == "server"){
                                        account->setServer((char*)event.data.scalar.value);
                                    } else if(tag == "port"){
                                        account->setPort(atoi((char*)event.data.scalar.value));
                                    } else if(tag == "user"){
                                        account->setUsername((char*)event.data.scalar.value);
                                    } else if(tag == "password"){
                                        account->setPassword((char*)event.data.scalar.value);
                                    } else if(tag == "ssl"){
                                        std::string value = (char*)event.data.scalar.value;
                                        if(value == "True"){
                                            account->setSSL(true);
                                        } else {
                                            account->setSSL(false);
                                        }
                                    } else if(tag == "default"){
                                        std::string value = (char*)event.data.scalar.value;
                                        if(value == "True"){
                                            account->setDefault(true);
                                        } else {
                                            account->setDefault(false);
                                        }
                                    }
                                }

                                yaml_event_delete(&event);

                            }
                            data_loop_done = 0;
                        } else if(event.type == YAML_SCALAR_EVENT){
                            account = new Account();
                            //INFO("ConfigManager::readConfigurationFiles :ACC_LOOP " << event.data.scalar.value);
                            account->setName((char*)event.data.scalar.value);
                            m_acc_list.push_back(account);

                        } else if(event.type == YAML_MAPPING_END_EVENT){
                            acc_loop_done = true;
                        }
                        yaml_event_delete(&event);

                    }

                }

	            doc_loop_done = (event.type == YAML_STREAM_END_EVENT);

	            yaml_event_delete(&event);

	        }

            yaml_parser_delete(&parser);
            fclose(file);



        } else {
            ERROR("ConfigManager: Could not initialize YAML Parser!");
        }
    } else {
        ERROR("ConfigManager: File " << accountsfile << " couldn't be opened!");
    }
}



void ConfigManager::readBaseConfiguration(){
    std::string configfile = m_homedir + "/.vilari_mail/config.yml";

    int doc_loop_done = 0;
    int acc_loop_done = 0;
    int data_loop_done = 0;

    std::string tag;
    int error = 0;

    yaml_parser_t parser;
    yaml_event_t event;

    FILE* file = fopen(configfile.c_str(), "rb");
    if(file != NULL){
        if(yaml_parser_initialize(&parser)){
            yaml_parser_set_input_file(&parser, file);

            while (!doc_loop_done)
	        {
	            if (!yaml_parser_parse(&parser, &event)) {
	                error = 1;
	                break;
	            }

                //handleYamlEvent(&event);
                if(event.type == YAML_SCALAR_EVENT){
                    //INFO("ConfigManager::readConfigurationFiles:  DATALOOP TAG " << event.data.scalar.value);
                    tag = (char*)event.data.scalar.value;
                    if (tag == "messagesperpage"){
                        yaml_event_delete(&event);
                        if (!yaml_parser_parse(&parser, &event)) {
                            error = 1;
                            break;
                        }
                        m_messagesperpage = atoi((char*)event.data.scalar.value);

                    }
                }

	            doc_loop_done = (event.type == YAML_STREAM_END_EVENT);

	            yaml_event_delete(&event);

	        }

        } else {
            ERROR("ConfigManager: Could not initialize YAML Parser!");
        }
    } else {
        ERROR("ConfigManager: File " << configfile << " couldn't be opened!");
    }
}

void ConfigManager::readConfigurationFiles(){

    readBaseConfiguration();

    readImapAccounts();

}


// TESTING YAML
void ConfigManager::handleYamlEvent(yaml_event_t* event){
    switch(event->type){
        case YAML_NO_EVENT:

            break;
        case YAML_STREAM_START_EVENT:
            INFO("YAML_STREAM_START_EVENT");
            break;
        case YAML_STREAM_END_EVENT:
            INFO("YAML_STREAM_END_EVENT");
            break;
        case YAML_DOCUMENT_START_EVENT:
            INFO("YAML_DOCUMENT_START_EVENT");
            break;
        case YAML_DOCUMENT_END_EVENT:
            INFO("YAML_DOCUMENT_END_EVENT");
            break;
        case YAML_ALIAS_EVENT:
            INFO("YAML_ALIAS_EVENT");
            break;
        case YAML_SCALAR_EVENT:
            INFO("YAML_SCALAR_EVENT");

            if (event->data.scalar.tag != NULL){
                INFO("    - tag: " << event->data.scalar.tag);
            } else {
                INFO("    - tag: NULL");
            }

            if (event->data.scalar.anchor != NULL){
                INFO("    - anchor: " << event->data.scalar.anchor);
            } else {
                INFO("    - anchor: NULL");
            }


            if (event->data.scalar.value != NULL){
                INFO("    - value: " << event->data.scalar.value);
            } else {
                INFO("    - value: NULL");
            }
            break;
        case YAML_SEQUENCE_START_EVENT:
            INFO("YAML_SEQUENCE_START_EVENT");
            break;
        case YAML_SEQUENCE_END_EVENT:
            INFO("YAML_SEQUENCE_END_EVENT");
            break;
        case YAML_MAPPING_START_EVENT:
            INFO("YAML_MAPPING_START_EVENT");
            if (event->data.mapping_start.tag != NULL){
                INFO("    - tag: " << event->data.mapping_start.tag);
            } else {
                INFO("    - tag: NULL");
            }

            if (event->data.mapping_start.anchor != NULL){
                INFO("    - anchor: " << event->data.mapping_start.anchor);
            } else {
                INFO("    - anchor: NULL");
            }


            break;
        case YAML_MAPPING_END_EVENT:
            INFO("YAML_MAPPING_END_EVENT");
            break;
    }

}





