/*
 *      IMAP.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAP_H
#define IMAP_H

#include "commons.h"

#include "ConfigManager.h"
#include "Folder.h"
#include "Header.h"
#include "Message.h"

#include <libetpan/mailimap.h>

class IMAP
{
    public:
        static IMAP* getInstance();
        static void destroy();


        bool connect(const std::string& host,
                     const int port,
                     bool useSSL,
                     const std::string& username,
                     const std::string& password);
        void disconnect();

        // Folder operation
        std::list< Folder* > getFolderList();
        void updateFolderStatus(Folder* folder);
        void updateFolderListStatus(std::list< Folder* > folderlist);

        void toggleReadFlag(Header* header);
        void markFolderRead(Folder* folder);


        std::list< Header* > getHeaderList(Folder* folder, int index);

        Message* getMessage(Header* header);



    protected:
    private:
         IMAP();
         IMAP(const IMAP& imap);
         virtual ~IMAP();
         static IMAP* m_instance;

         void parseHeader(mailimap_msg_att_item* att_item, Header** header);
         void parseEnvelope(mailimap_envelope* envelope, Header** header);
         void parseDate(mailimap_date_time* date, Header** header);

         //void parseBody(mailimap_msg_att_static* statdata);
         void parseBodySection(mailimap_msg_att_body_section* body_sec, Message* message);

         //void parseSinglePartBody(mailimap_body_type_1part* body);
         //void parseMultiPartBody(mailimap_body_type_mpart* body);


         mailimap* m_imapsession;
         int error;
         bool m_connectstatus;

         std::list<Folder*> m_folderlist;

};

#endif // MAILBACKEND_H
