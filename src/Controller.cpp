/*
 *      Controller.cpp
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Controller.h"

#include <commons.h>

Controller* Controller::m_instance = NULL;

Controller* Controller::getInstance(){
    if( m_instance == NULL ){
      m_instance = new Controller();
    }
    return m_instance;
}

void Controller::destroy(){
    if( m_instance ){
      delete m_instance;
      m_instance = 0;
    }
}


Controller::Controller()
{
    MEMDEBUG("CONTROLLER instance created!");
    m_currentmessage = NULL;
}

void Controller::runApp(){
    m_mainwin = new MainWindow();

    elm_run();

}


void Controller::quit(){
    IMAP::getInstance()->disconnect();
    elm_exit();

}

Controller::~Controller()
{
    delete m_mainwin;
}

Controller::Controller(const Controller& imap){

}


void Controller::setFolderList(std::list< Folder* > folderlist){
    //iterate over existing list and delete the folder objects
    std::list< Folder* >::iterator it;
    for (it=m_currentfolderlist.begin(); it != m_currentfolderlist.end(); it++){
         delete (*it);
    }

    // set the new list
    m_currentfolderlist = folderlist;
}


void Controller::setHeaderList(std::list< Header* > headerlist){
    std::list< Header* >::iterator it;
    for (it=m_currentheaderlist.begin(); it != m_currentheaderlist.end(); it++){
         delete (*it);
    }

    m_currentheaderlist = headerlist;
}

void Controller::connect(Account* account){

    IMAP* imapconnection = IMAP::getInstance();
    DEBUG("Controller::connect connecting...");
    imapconnection->connect(account->getServer(),
                            account->getPort(),
                            account->useSSL(),
                            account->getUsername(),
                            account->getPassword());

    DEBUG("Controller::connect get list of folders...");
    setFolderList(imapconnection->getFolderList());

    DEBUG("Controller::connect check status of all folders in the list...");
    imapconnection->updateFolderListStatus(m_currentfolderlist);

    DEBUG("Controller::connect show the folderwindow...");
    m_mainwin->displayFolderWindow();

    DEBUG("Controller::connect let the folderwindow display the folderlist...");
    m_mainwin->updateFolderWindow(m_currentfolderlist);

}


void Controller::goBack(){
    m_mainwin->displayLastContentWindow();
}

void Controller::openHeaderList(Folder* folder){
    DEBUG("Controller::showHeaderList: called");
    m_mainwin->displayMessageListWindow();

    updateHeaderList(folder, 0);
    DEBUG("Controller::showHeaderList: leaving");
}


void Controller::updateHeaderList(Folder* folder, int index){
    DEBUG("Controller::updateHeaderList called");
    int messagesperpage = ConfigManager::getInstance()->getMessagesPerPage();

    int temp_index_from = index;
    int temp_index_to = index + messagesperpage;

    int messagecount = folder->getMessageCount();

    if(messagecount <= messagesperpage){
        temp_index_from = 0;
        temp_index_to = messagesperpage;
    } else if(temp_index_from+messagesperpage > messagecount) {
        temp_index_from = messagecount-messagesperpage;
        temp_index_to = temp_index_from+messagesperpage;
    } else if (temp_index_from < 0){
        temp_index_from = 0;
        if(messagecount <= messagesperpage){
            temp_index_to = messagecount;
        } else {
            temp_index_to = messagesperpage;
        }
    }


    DEBUG("Controller::showHeaderList: get the list of headers used index: " << temp_index_from << " (" << index << ")");
    IMAP* imapconnection = IMAP::getInstance();

    std::list<Header*> headerlist = imapconnection->getHeaderList(folder, temp_index_from);

    setHeaderList(headerlist);
    m_current_index_from = temp_index_from;
    m_current_index_to = temp_index_to;
    m_current_folder = folder;

    refreshHeaderListGUI();

    DEBUG("Controller::updateHeaderList leaving");
}


void Controller::refreshHeaderListGUI(){
    DEBUG("Controller::refreshHeaderListGUI: called");
    m_mainwin->updateMessageListWindow(m_current_folder,
                                       m_currentheaderlist,
                                       m_current_index_from,
                                       m_current_index_to,
                                       m_current_folder->getUnreadMessageCount(),
                                       m_current_folder->getMessageCount());

    DEBUG("Controller::refreshHeaderListGUI leaving");
}


void Controller::showMessage(Header* header){

    DEBUG("Controller::showMessage getting Message for MessageNum: " << header->getMessageNum());
    IMAP* imapconnection = IMAP::getInstance();

    if(m_currentmessage != NULL){
        delete m_currentmessage;
    }

    DEBUG("Controller::showMessage fetch the message");
    Message* message = imapconnection->getMessage(header);

    DEBUG("Controller::showMessage show up the message display window");
    m_mainwin->displayMessageDisplayWindow();

    DEBUG("Controller::showMessage fill the message display window with the message content");
    m_mainwin->updateMessageDisplayWindow(message);

    DEBUG("Controller::showMessage leaving");
}


void Controller::toggleReadFlag(Header* header){
    DEBUG("Controller::toggleReadFlag called");
    IMAP* imapconnection = IMAP::getInstance();

    imapconnection->toggleReadFlag(header);

    // refresh the messagelist window
    refreshHeaderListGUI();

    // and refresh of the folderlist window
    m_mainwin->updateFolderWindow(m_currentfolderlist);

    DEBUG("Controller::toggleReadFlag leaving");
}

void Controller::refreshSelectedFolder(Folder* folder){
    DEBUG("Controller::refreshSelectedFolder called");
    IMAP* imapconnection = IMAP::getInstance();

    imapconnection->updateFolderStatus(folder);

    DEBUG("Controller::refreshSelectedFolder ... refreshing gui");
    m_mainwin->updateFolderWindow(m_currentfolderlist);

    DEBUG("Controller::refreshSelectedFolder leaving");
}


void Controller::refreshAllFolders(){
    DEBUG("Controller::refreshAllFolders called");
    IMAP* imapconnection = IMAP::getInstance();

    imapconnection->updateFolderListStatus(m_currentfolderlist);
    DEBUG("Controller::refreshAllFolders ... refreshing gui");
    m_mainwin->updateFolderWindow(m_currentfolderlist);

    DEBUG("Controller::refreshAllFolders leaving");
}

void Controller::markFolderRead(Folder* folder){
    DEBUG("Controller::markFolderRead called");

    IMAP* imapconnection = IMAP::getInstance();

    imapconnection->markFolderRead(folder);

    DEBUG("Controller::refreshAllFolders ... refreshing gui");
    m_mainwin->updateFolderWindow(m_currentfolderlist);

    DEBUG("Controller::markFolderRead leaving");
}

