/*
 *      IMAP.cpp
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "IMAP.h"


IMAP* IMAP::m_instance = NULL;

IMAP* IMAP::getInstance(){
    if( !m_instance ){
      m_instance = new IMAP();
    }
    return m_instance;
}



void IMAP::destroy(){
    if( m_instance ){
      delete m_instance;
      m_instance = 0;
    }
}

IMAP::IMAP()
{
    // Parameters are for registering a progess callback
    // maybe later implemented, currently none
    m_imapsession = mailimap_new(0, NULL);
    m_connectstatus = false;


}


IMAP::IMAP(const IMAP& imap)
{
}


IMAP::~IMAP()
{

    mailimap_free(m_imapsession);
}

bool IMAP::connect(const std::string& host,
                   const int port,
                   bool useSSL,
                   const std::string& username,
                   const std::string& password
                   ){

    int result;
    if (true == useSSL){
        result = mailimap_ssl_connect(m_imapsession, host.c_str(), port);
    } else {
        result = mailimap_socket_connect(m_imapsession, host.c_str(), port);
    }

    if (MAILIMAP_NO_ERROR_AUTHENTICATED == result || MAILIMAP_NO_ERROR_NON_AUTHENTICATED == result) {
        INFO("Connection established!");
    } else {
        ERROR("Error connectiong to IMAP Storage - errorcode: " << result << " !");
        return false;
    }

    result = mailimap_login(m_imapsession, username.c_str(), password.c_str());
    if (MAILIMAP_NO_ERROR == result){
        INFO("Logged in");
    } else {
        ERROR("Failure while logging in to server! Code: " << result);
        return false;
    }

    m_connectstatus = true;
    return true;
}


void IMAP::disconnect(){
    if(m_connectstatus){
        int result = mailimap_logout(m_imapsession);
        if (MAILIMAP_NO_ERROR == result){
            INFO("Logged out");
        } else {
            ERROR("Failure while logging out from server! Code: " << result);
        }
    } else {
        INFO("No disconnect needed, not connected...");
    }
}

void IMAP::parseEnvelope(mailimap_envelope* envelope, Header** header){

    clistiter* iter;
    // get the from addresses
    if (envelope->env_to->to_list != NULL){
        for(iter = clist_begin(envelope->env_from->frm_list) ; iter != NULL ; iter = clist_next(iter) ) {
            mailimap_address* address = (mailimap_address*)clist_content(iter);
            if (address->ad_host_name != NULL && address->ad_mailbox_name != NULL){

                Address* newaddress = new Address();

                std::ostringstream addstream;
                addstream << address->ad_mailbox_name << "@" << address->ad_host_name;
                newaddress->setAddress(addstream.str());

                DEBUG2("IMAP::parseEnvelope - FROM address found : " << addstream.str());

                if (address->ad_personal_name != NULL){
                    newaddress->setDisplayName(address->ad_personal_name);
                    DEBUG2("\t\t\t\tDisplayname = " << newaddress->getDisplayName());
                }
                (*header)->addFrom(newaddress);

            }
        }
    }


    // get the receiver
    if (envelope->env_to->to_list != NULL){
        for(iter = clist_begin(envelope->env_to->to_list) ; iter != NULL ; iter = clist_next(iter) ) {
            mailimap_address* address = (mailimap_address*)clist_content(iter);

            if (address->ad_host_name != NULL && address->ad_mailbox_name != NULL){

                Address* newaddress = new Address();

                std::ostringstream addstream;
                addstream << address->ad_mailbox_name << "@" << address->ad_host_name;
                newaddress->setAddress(addstream.str());

                DEBUG2("IMAP::parseEnvelope - TO address found : " << addstream.str());

                if (address->ad_personal_name != NULL){
                    newaddress->setDisplayName(address->ad_personal_name);
                    DEBUG2("\t\t\t\tDisplayname = " << newaddress->getDisplayName());
                }
                (*header)->addTo(newaddress);

            }
        }
    }

    // get the replyto addresses
    if (envelope->env_reply_to->rt_list != NULL){
        for(iter = clist_begin(envelope->env_reply_to->rt_list) ; iter != NULL ; iter = clist_next(iter) ) {
            mailimap_address* address = (mailimap_address*)clist_content(iter);

            if (address->ad_host_name != NULL && address->ad_mailbox_name != NULL){

                Address* newaddress = new Address();

                std::ostringstream addstream;
                addstream << address->ad_mailbox_name << "@" << address->ad_host_name;
                newaddress->setAddress(addstream.str());

                DEBUG2("IMAP::parseEnvelope - REPLY-TO address found : " << addstream.str());

                if (address->ad_personal_name != NULL){
                    newaddress->setDisplayName(address->ad_personal_name);
                    DEBUG2("\t\t\t\tDisplayname = " << newaddress->getDisplayName());
                }
                (*header)->addReplyTo(newaddress);

            }
        }
    }


    // get the cc addresses
    if (envelope->env_cc->cc_list != NULL){
        for(iter = clist_begin(envelope->env_cc->cc_list) ; iter != NULL ; iter = clist_next(iter) ) {
            mailimap_address* address = (mailimap_address*)clist_content(iter);

            if (address->ad_host_name != NULL && address->ad_mailbox_name != NULL){

                Address* newaddress = new Address();

                std::ostringstream addstream;
                addstream << address->ad_mailbox_name << "@" << address->ad_host_name;
                newaddress->setAddress(addstream.str());

                DEBUG2("IMAP::parseEnvelope - CC address found : " << addstream.str());

                if (address->ad_personal_name != NULL){
                    newaddress->setDisplayName(address->ad_personal_name);
                    DEBUG2("\t\t\t\tDisplayname = " << newaddress->getDisplayName());
                }
                (*header)->addCC(newaddress);

            }
        }
    }

    // the subject
    if (envelope->env_subject != NULL){
          DEBUG2("IMAP::parseEnvelope - Subject found : " << envelope->env_subject );
          (*header)->setSubject(envelope->env_subject);
    } else {
          DEBUG2("IMAP::parseEnvelope - No Subject found ! ");
          (*header)->setSubject("# Without Subject #");
    }


    if (envelope->env_date != NULL){
          DEBUG2("IMAP::parseEnvelope - Date found : " << envelope->env_date );
          (*header)->setDate(envelope->env_date);
    } else {
          DEBUG2("IMAP::parseEnvelope - No Date found ! ");
          (*header)->setDate("# No Date found #");
    }
}

void IMAP::parseDate(mailimap_date_time* date, Header** header){
    //mailimap_date_time* date = statdata->att_data.att_internal_date;

    // The internal date is not very usefull because it contains no information
    // about timezone - better to use the env_date from the header_envelope
    std::ostringstream datestream;
    datestream << date->dt_year << "-" << date->dt_month << "-" << date->dt_day << " ";
    datestream << date->dt_hour << ":" << date->dt_min;
    DEBUG2("\t\tDate = " << datestream.str());

}

void IMAP::parseHeader(mailimap_msg_att_item* att_item, Header** header){
    if( att_item->att_type == MAILIMAP_MSG_ATT_ITEM_DYNAMIC ){
        DEBUG2("parseHeader:DYNAMIC Data in att_item found");
        mailimap_msg_att_dynamic* dyndata = att_item->att_data.att_dyn;
        if (dyndata != NULL){
            clist * att_list = dyndata->att_list;
            if (att_list != NULL){ // NULL happens when no flags are set I think
                clistiter* iter;
                for(iter = clist_begin(att_list) ; iter != NULL ; iter = clist_next(iter) ) {
                    mailimap_flag_fetch* flag_fetch = (mailimap_flag_fetch*)clist_content(iter);
                    DEBUG2("parseHeader:FLAGS: flagtype = " << flag_fetch->fl_type);
                    if (flag_fetch->fl_type == MAILIMAP_FLAG_FETCH_ERROR){
                        ERROR("parseHeader:DYNAMIC : ParseError !!");
                    } else {
                        mailimap_flag* flag = flag_fetch->fl_flag;
                        switch(flag->fl_type){
                            case MAILIMAP_FLAG_ANSWERED:
                                DEBUG2("Flag ANSWERED found!");
                                //ToDo: Implement
                                break;
                            case MAILIMAP_FLAG_FLAGGED:
                                DEBUG2("Flag FLAGGED found!");
                                //ToDo: Implement
                                break;
                            case MAILIMAP_FLAG_DELETED:
                                DEBUG2("Flag DELETED found!");
                                //ToDo: Implement
                                break;
                            case MAILIMAP_FLAG_SEEN:
                                DEBUG2("Flag SEEN found!");
                                (*header)->setReadFlag(true);
                                break;
                            case MAILIMAP_FLAG_DRAFT:
                                DEBUG2("Flag DRAFT found!");
                                //ToDo: Implement
                                break;
                            case MAILIMAP_FLAG_KEYWORD:
                                DEBUG2("Flag KEYWORD found!");
                                //ToDo: Implement
                                break;
                            case MAILIMAP_FLAG_EXTENSION:
                                DEBUG2("Flag EXTENSION found!");
                                //ToDo: Implement
                                break;
                            default: ERROR("FLAG not recognized: should not happen");

                        } // switch(flag->fl_type)

                    } // else  // if (flag_fetch->fl_type == MAILIMAP_FLAG_FETCH_ERROR){

                } // for(iter = clist_begin(att_list) ; iter != NULL ; iter = clist_next(iter) ) {

             } else {
                 DEBUG2("parseHeader:DYNAMIC: attlist is NULL!");
             }
        } else {// if (dyndata != NULL){
            DEBUG2("parseHeader:DYNAMIC: dyndata is NULL!");
        }
    } else if ( att_item->att_type == MAILIMAP_MSG_ATT_ITEM_STATIC ){
        DEBUG2("parseHeader:STATIC Data in att_item found");
        mailimap_msg_att_static* statdata = att_item->att_data.att_static;

        switch(statdata->att_type){
            case MAILIMAP_MSG_ATT_ENVELOPE:
                DEBUG2("\t\tMAILIMAP_MSG_ATT_ENVELOPE ATT found");
                parseEnvelope(statdata->att_data.att_env, header);
                break;
            case MAILIMAP_MSG_ATT_INTERNALDATE:
                DEBUG2("\t\tMAILIMAP_MSG_ATT_INTERNALDATE ATT found");
                //see comment in parseDate
                //parseDate(statdata->att_data.att_internal_date , header);
                break;
            case MAILIMAP_MSG_ATT_RFC822_SIZE:
                DEBUG2("\t\tMAILIMAP_MSG_ATT_RFC822_SIZE ATT found");
                break;
            default: DEBUG2("parseHeader:STATIC Type "  << statdata->att_type << " found but not yes implemented");
        }

        //DEBUG2("parseHeader:STATIC Type : "  << statdata->att_type);



    } else {
        ERROR("Unexpected Type for mailimap_msg_att_item* att_item in IMAP::parseHeader! -> "  << att_item->att_type);
    }

}


std::list<Header*> IMAP::getHeaderList(Folder* folder, int index){

    std::list<Header*> headerlist;

    // first select the folder
    int result = mailimap_select(m_imapsession, folder->getFoldername().c_str());
    if (MAILIMAP_NO_ERROR == result){
            DEBUG2("Folder Selection ok");
            // searchkey for undeleted messages
            mailimap_search_key* searchkey = mailimap_search_key_new(MAILIMAP_SEARCH_KEY_UNDELETED,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL
                                                                    );

            clist* messagenumbers;

            result = mailimap_search(m_imapsession, DEFAULT_CHARSET, searchkey, &messagenumbers);
            if (MAILIMAP_NO_ERROR == result){
                DEBUG2("Search successful");

                int messagecount = clist_count(messagenumbers);
                int tempmessagesperpage = ConfigManager::getInstance()->getMessagesPerPage();

                if (ConfigManager::getInstance()->getMessagesPerPage() > messagecount){
                    index = 0;
                    tempmessagesperpage = messagecount;
                } else if (index >= messagecount){
                    index = messagecount-ConfigManager::getInstance()->getMessagesPerPage();
                } else if (index < 0){
                    index = 0;
                } else if (messagecount-index < tempmessagesperpage){
                    index = messagecount-tempmessagesperpage;
                }


                clistiter* iter;
                if (index == 0){
                    iter = clist_end(messagenumbers);
                } else {
                    iter = clist_nth(messagenumbers, messagecount-index);
                }

                int run = 0;
                while (run < tempmessagesperpage){
                    if (iter != NULL){
                        uint32_t* messagenum = (uint32_t*)clist_content(iter);
                        DEBUG2("MSG NUM: " << *messagenum);
                        Header* header = new Header(folder, *messagenum);

                        //mailimap_fetch_att* fetch_att = mailimap_fetch_att_new_rfc822_header();
                        mailimap_fetch_type* fetchtype = mailimap_fetch_type_new_all();

                        mailimap_set* msgset =  mailimap_set_new_single(*messagenum);


                        clist* fetchresult;

                        int result = mailimap_fetch(m_imapsession, msgset, fetchtype, &fetchresult);
                        if (MAILIMAP_NO_ERROR == result){
                            DEBUG2("Fetch OK for MessageNum: " << *messagenum);
                            clistiter* iter;
                            for(iter = clist_begin(fetchresult) ; iter != NULL ; iter = clist_next(iter) ) {
                                mailimap_msg_att* msg_att_struct = (mailimap_msg_att*)clist_content(iter);
                                clistiter* iter2;
                                for(iter2 = clist_begin(msg_att_struct->att_list) ; iter2 != NULL ; iter2 = clist_next(iter2) ) {
                                    mailimap_msg_att_item* att_item = (mailimap_msg_att_item*)clist_content(iter2);
                                    //fills the Header with data
                                    parseHeader(att_item, &header);
                                }

                            }
                            headerlist.push_back(header);
                            mailimap_fetch_list_free(fetchresult);

                        } else {
                            ERROR("Fetch failed for MessageNum: " << messagenum << " Code: " << result);
                        }


                        mailimap_fetch_type_free(fetchtype);
                        mailimap_set_free(msgset);

                    } else {
                        DEBUG2("MESSAGENUM Loop iter is NULL!");
                    }
                    iter = clist_previous(iter);
                    run++;
                }

            } else {
                ERROR("Failure searching for undeleted messages! Code: " << result);
            }


            mailimap_search_key_free(searchkey);
    } else {
         ERROR("Failure Selecting Folder! Code: " << result);
    }



    return headerlist;
}



std::list<Folder*> IMAP::getFolderList(){
      std::list<Folder*> return_folderlist;

      clist* mailboxlist;
      int result =  mailimap_list(m_imapsession, "", "*", &mailboxlist);

      if (MAILIMAP_NO_ERROR == result){
            DEBUG2("Fetched Folderlist");

            clistiter* iter;
            for(iter = clist_begin(mailboxlist) ; iter != NULL ; iter = clist_next(iter) ) {
                mailimap_mailbox_list * mailboxstruct = (mailimap_mailbox_list *)clist_content(iter);
                DEBUG2("Foldername : " << mailboxstruct->mb_name);
                Folder* folder = new Folder(mailboxstruct->mb_name);
                return_folderlist.push_back(folder);
            }

            mailimap_list_result_free(mailboxlist);

      } else {
            ERROR("Error fetching foldernames from server! Code: " << result);
      }

      return return_folderlist;
}


void IMAP::updateFolderStatus(Folder* folder){
    DEBUG2("IMAP::updateFolderStatus Updating Status (total and unread message count) for Folder: " << folder->getFoldername());


    clist* statusattributes = clist_new();
    int* argument1 = (int*) malloc(sizeof(int));
    int* argument2 = (int*) malloc(sizeof(int));
    *argument1 = MAILIMAP_STATUS_ATT_MESSAGES;
    *argument2 = MAILIMAP_STATUS_ATT_UNSEEN;
    clist_append(statusattributes, argument1); // for total messsages count
    clist_append(statusattributes, argument2);    // for unseen messages count
    mailimap_status_att_list* statusattlist = mailimap_status_att_list_new(statusattributes);

    mailimap_mailbox_data_status* statusresult;

    int result = mailimap_status(m_imapsession, folder->getFoldername().c_str(), statusattlist, &statusresult);
    if (MAILIMAP_NO_ERROR == result){
        DEBUG2("IMAP::updateFolderStatus Status retrieval successful!");

        clist* statuslist = statusresult->st_info_list;
        clistiter * iter;
        for(iter = clist_begin(statuslist) ; iter != NULL ; iter = clist_next(iter)) {
            mailimap_status_info* statusinfo = (mailimap_status_info*)clist_content(iter);
            if(MAILIMAP_STATUS_ATT_MESSAGES == statusinfo->st_att){
                folder->setMessageCount(statusinfo->st_value);
            } else if(MAILIMAP_STATUS_ATT_UNSEEN == statusinfo->st_att){
                folder->setUnreadMessageCount(statusinfo->st_value);
            } else {
                DEBUG2("IMAP::updateFolderStatus This shouldn't happen! Just ignoring this");
            }
        }
        DEBUG2("IMAP::updateFolderStatus Folderstatus now is: Messages: " << folder->getMessageCount() << " Unread: " << folder->getUnreadMessageCount());

        mailimap_mailbox_data_status_free(statusresult);

    } else {
        ERROR("IMAP::updateFolderStatus Failure getting status of folder " << folder->getFoldername() << " ! Code: " << result);
    }



    mailimap_status_att_list_free(statusattlist);
}


void IMAP::updateFolderListStatus(std::list< Folder* > folderlist){
    std::list< Folder* >::iterator it;
    for (it=folderlist.begin(); it != folderlist.end(); it++){
         updateFolderStatus((*it));
    }
}


/* Hauptsächlich Charsetkram also Metadaten.
void IMAP::parseSinglePartBody(mailimap_body_type_1part* body){
    if(body->bd_type == MAILIMAP_BODY_TYPE_1PART_TEXT){
        mailimap_body_type_text* text = body->bd_data.bd_type_text;
        DEBUG2("Messagetext = " << text->bd_media_text); // z.B. : "plain"

        mailimap_body_fields* body_fields = text->bd_fields;
        mailimap_body_fld_param* fld_parm = body_fields->bd_parameter;
        if(fld_parm != NULL){
            clistiter* iter;
            for(iter = clist_begin(fld_parm->pa_list) ; iter != NULL ; iter = clist_next(iter) ) {
                mailimap_single_body_fld_param* parm = (mailimap_single_body_fld_param*)clist_content(iter);
                if(parm->pa_name != NULL){
                    DEBUG2("IMAP::parseSinglePartBody                     pa_name : " << parm->pa_name);  // z.B.: "charset"
                } else {
                    DEBUG2("IMAP::parseSinglePartBody                     pa_name : NULL");
                }

                if(parm->pa_value != NULL){
                    DEBUG2("IMAP::parseSinglePartBody                     pa_value : " << parm->pa_value); // z.B.: "ISO-8859-1"
                } else {
                    DEBUG2("IMAP::parseSinglePartBody                     pa_value : NULL");
                }

            } // for(iter = clist_begin(fld_parm->pa_list) ; iter != NULL ; iter = clist_next(iter) ) {
        } else {
            ERROR("IMAP::parseSinglePartBody FLD_PARM is NULL");
        }

        //DEBUG2("BODY Description = " << body_fields->bd_description);


    } else {
        DEBUG2("IMAP::parseSinglePartBody: TYPE but not yet supported : " << body->bd_type);
    }

}
*/
/*
void IMAP::parseMultiPartBody(mailimap_body_type_mpart* body){
    DEBUG2("IMAP::parseMultiPartBody: ");
}
*/

/*
void IMAP::parseBody(mailimap_msg_att_static* statdata){

    if(statdata->att_data.att_body == NULL){
        ERROR("IMAP::parseBody: body is NULL");
    } else {
        mailimap_body* body = statdata->att_data.att_body;
        if(body->bd_type == MAILIMAP_BODY_1PART){
            DEBUG2("IMAP::parseBody: Body is a SinglePart Message");
            //parseSinglePartBody(body->bd_data.bd_body_1part);
        } else if (body->bd_type == MAILIMAP_BODY_MPART){
            DEBUG2("IMAP::parseBody: Body is a MultiPart Message");
            parseMultiPartBody(body->bd_data.bd_body_mpart);
        } else {
            ERROR("IMAP::parseBody: unexpected body type found : " << body->bd_type);
        }

    }

}*/


void IMAP::parseBodySection(mailimap_msg_att_body_section* body_sec, Message* message){
    if(body_sec->sec_body_part != NULL){
        DEBUG2("IMAP::parseBodySection  body_part is "  << body_sec->sec_body_part);
        //DEBUG2("IMAP::parseBodySection  body_part found");
        message->setText(body_sec->sec_body_part);
    } else {
        DEBUG2("IMAP::parseBodySection  body_part is NULL");
    }
    /*mailimap_section* section = body_sec->sec_section;
    if (section != NULL){
        if (section->sec_spec != NULL){
            switch(section_spec->sec_type)
            {
                  case MAILIMAP_SECTION_SPEC_SECTION_MSGTEXT:
                        section_spec->sec_data.sec_msgtext
                        break;
                  case MAILIMAP_SECTION_SPEC_SECTION_PART:
                        //section_spec->sec_data.sec_part);
                        //if (section_spec->sec_text != NULL)
                         // mailimap_section_text_print(section_spec->sec_text);
                        break;
            }
        } else {
            ERROR("IMAP::parseBodySection section->sec_spec is NULL");
        }

    } else {
        ERROR("IMAP::parseBodySection body_sec->sec_section is NULL");
    }
    */
}

Message* IMAP::getMessage(Header* header){
    if(header != NULL){
        Folder* folder = header->getFolder();
        if (folder != NULL){
            int result = mailimap_select(m_imapsession, folder->getFoldername().c_str());
            if (MAILIMAP_NO_ERROR == result){
                    DEBUG2("IMAP::getMessage: Folder Selection ok -> " << folder->getFoldername());

                    //mailimap_fetch_att* fetch_att = mailimap_fetch_att_new_bodystructure();

                    //mailimap_fetch_att* fetch_att = mailimap_fetch_att_new_body();

                    mailimap_section* section = mailimap_section_new_text();

                    mailimap_fetch_att* fetch_att = mailimap_fetch_att_new_body_section(section);
                    mailimap_fetch_type* fetchtype = mailimap_fetch_type_new_fetch_att(fetch_att);

                    mailimap_set* msgset =  mailimap_set_new_single(header->getMessageNum());

                    clist* fetchresult;

                    int result = mailimap_fetch(m_imapsession, msgset, fetchtype, &fetchresult);
                    if (MAILIMAP_NO_ERROR == result){
                        Message* message = new Message(header);
                        DEBUG2("IMAP::getMessage:Fetch OK for MessageNum: " << header->getMessageNum());
                        clistiter* iter;
                        for(iter = clist_begin(fetchresult) ; iter != NULL ; iter = clist_next(iter) ) {
                            mailimap_msg_att* msg_att_struct = (mailimap_msg_att*)clist_content(iter);
                            clistiter* iter2;
                            for(iter2 = clist_begin(msg_att_struct->att_list) ; iter2 != NULL ; iter2 = clist_next(iter2) ) {
                                mailimap_msg_att_item* att_item = (mailimap_msg_att_item*)clist_content(iter2);
                                if( att_item->att_type == MAILIMAP_MSG_ATT_ITEM_DYNAMIC ){
                                    // only for flags
                                    ERROR("IMAP::getMessage:DYNAMIC Data in att_item found" << std::endl << "This shouldn't happen in this method!");
                                } else if ( att_item->att_type == MAILIMAP_MSG_ATT_ITEM_STATIC ){
                                    // this is the expected att_type for the above search criteria
                                    DEBUG2("IMAP::getMessage:STATIC Data in att_item found");
                                    mailimap_msg_att_static* static_data = att_item->att_data.att_static;
                                    if (static_data->att_type == MAILIMAP_MSG_ATT_BODY_SECTION){
                                        DEBUG2("IMAP::getMessage: MAILIMAP_MSG_ATT_BODY_SECTION");
                                        mailimap_msg_att_body_section* body_sec = static_data->att_data.att_body_section;
                                        if(body_sec != NULL){
                                            parseBodySection(body_sec, message);
                                        } else {
                                            ERROR("IMAP::getMessage: BODY Section is NULL!");
                                        }


                                    } else {
                                        ERROR("IMAP::getMessage: DataType = " << static_data->att_type << " this is not excpected!");
                                    }



                                } else {
                                    ERROR("IMAP::getMessage:Unexpected Type for mailimap_msg_att_item* att_item -> "  << att_item->att_type);
                                }

                            } // for(iter2 = clist_begin(msg_att_struct->att_list) ; iter2 != NULL ; iter2 = clist_next(iter2) ) {
                        } // for(iter = clist_begin(fetchresult) ; iter != NULL ; iter = clist_next(iter) ) {


                        mailimap_fetch_list_free(fetchresult);
                        return message;

                    } else {
                        ERROR("IMAP::getMessage: Fetch failed! MessageNum" << header->getMessageNum() << " Code:" << result);
                        return NULL;
                    }

                    mailimap_fetch_type_free(fetchtype);
                    mailimap_set_free(msgset);


            } else {
                ERROR("IMAP::getMessage: Selection of Folder failed! " << folder->getFoldername() << " Code: " << result);
                return NULL;
            }
        } else {
            ERROR("IMAP::getMessage: Folder is NULL, this shouldn't happen!");
            return NULL;
        }
    } else {
        ERROR("IMAP::getMessage: Header is NULL, this shouldn't happen!");
        return NULL;
    }
}



void IMAP::toggleReadFlag(Header* header){

    int sign;
    if (header->getReadFlag()){
        sign = -1; // means: remove flag
        header->setReadFlag(false);
        header->getFolder()->setUnreadMessageCount(header->getFolder()->getUnreadMessageCount()+1);
    } else {
        sign = +1;  // means: add flag
        header->setReadFlag(true);
        header->getFolder()->setUnreadMessageCount(header->getFolder()->getUnreadMessageCount()-1);
    }

    clist* flaglist = clist_new();

    mailimap_flag* seenflag = mailimap_flag_new(MAILIMAP_FLAG_SEEN, NULL, NULL);

    clist_append(flaglist, seenflag);

    mailimap_flag_list* mimapflaglist = mailimap_flag_list_new(flaglist);

    mailimap_store_att_flags* att_flags = mailimap_store_att_flags_new(sign, 1, mimapflaglist);

    mailimap_set* msgset =  mailimap_set_new_single(header->getMessageNum());

    mailimap_store(m_imapsession, msgset, att_flags);

    mailimap_set_free(msgset);
    mailimap_store_att_flags_free(att_flags);

}


void IMAP::markFolderRead(Folder* folder){
    // first select the folder
    DEBUG2("IMAP::markFolderRead - selecting folder: " << folder->getFoldername());
    int result = mailimap_select(m_imapsession, folder->getFoldername().c_str());
    if (MAILIMAP_NO_ERROR == result){
            DEBUG2("IMAP::markFolderRead - Folder Selection ok - now searching for unread messages");
            // searchkey for undeleted messages
            mailimap_search_key* searchkey = mailimap_search_key_new(MAILIMAP_SEARCH_KEY_UNSEEN,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL, NULL, NULL, NULL,
                                                                     NULL
                                                                    );

            clist* messagenumbers;

            result = mailimap_search(m_imapsession, DEFAULT_CHARSET, searchkey, &messagenumbers);
            if (MAILIMAP_NO_ERROR == result){
                DEBUG2("IMAP::markFolderRead - Search successful");

                clistiter* iter;
                for(iter = clist_begin(messagenumbers) ; iter != NULL ; iter = clist_next(iter) ) {
                    uint32_t* messagenum = (uint32_t*)clist_content(iter);
                    DEBUG2("\t\tIMAP::markFolderRead : marking message with num: " << *messagenum << " read!");

                    clist* flaglist = clist_new();

                    mailimap_flag* seenflag = mailimap_flag_new(MAILIMAP_FLAG_SEEN, NULL, NULL);

                    clist_append(flaglist, seenflag);

                    mailimap_flag_list* mimapflaglist = mailimap_flag_list_new(flaglist);

                    mailimap_store_att_flags* att_flags = mailimap_store_att_flags_new(1, 1, mimapflaglist);

                    mailimap_set* msgset =  mailimap_set_new_single(*messagenum);

                    mailimap_store(m_imapsession, msgset, att_flags);

                    mailimap_set_free(msgset);
                    mailimap_store_att_flags_free(att_flags);

                    folder->setUnreadMessageCount(folder->getUnreadMessageCount()-1);
                }

            } else {
                ERROR("IMAP::markFolderRead - Search failed!");
            }
    } else {
        ERROR("IMAP::markFolderRead - Folder selection failed!");
    }

}



