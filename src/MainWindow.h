/*
 *      MainWindow.h
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <commons.h>
#include <Elementary.h>
#include <Content.h>

class AccountWindow;
#include <AccountWindow.h>

class FolderWindow;
#include <FolderWindow.h>

class MessageListWindow;
#include <MessageListWindow.h>

class MessageDisplayWindow;
#include <MessageDisplayWindow.h>

#include <Folder.h>


class MainWindow
{
    public:
        MainWindow();
        virtual ~MainWindow();


        void displayLastContentWindow();

        void displayAccWindow();

        void displayFolderWindow();

        void displayMessageListWindow();

        void displayMessageDisplayWindow();



        void updateFolderWindow(std::list< Folder* > folderlist);
        void updateMessageListWindow(Folder* folder,
                                     std::list<Header*> headerlist,
                                     int index_from,
                                     int index_to,
                                     int folder_unread,
                                     int folder_total);
        void updateMessageDisplayWindow(Message* message);


    protected:
    private:
        static void win_del(void *data, Evas_Object *obj, void *event_info);


        void initWindows();

        void switchContent(Content* newcontent);

        void displayContent(Content* content);
        void removeContent(Content* content);
        Content* getLastContent();

        //Content* m_current_content;
        //Content* m_prev_content;

        std::list<Content*> m_contents;

        AccountWindow* m_acc_win;
        FolderWindow* m_folder_win;
        MessageListWindow* m_msg_lst_win;
        MessageDisplayWindow* m_msg_dsp_win;


        Evas_Object* m_window;


};

#endif // MAINWINDOW_H
