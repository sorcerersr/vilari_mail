/*
 *      AccountWindow.cpp
 *
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AccountWindow.h"

AccountWindow::AccountWindow(Evas_Object* win)  : Content(win)
{

    Evas_Object* fr0 = elm_frame_add(win);
    evas_object_size_hint_weight_set(fr0, 1.0, 0.0);
    evas_object_size_hint_align_set(fr0, -1.0, -1.0);

    elm_box_pack_end(m_content_box, fr0);
    elm_frame_label_set(fr0, "Accounts:");
    evas_object_show(fr0);

    Evas_Object* lb = elm_label_add(win);
    elm_label_label_set(lb, "Please choose an account from the<br>list below and click connect.");
    elm_frame_content_set(fr0, lb);
    evas_object_show(lb);

    m_hoversel = elm_hoversel_add(win);
    elm_hoversel_hover_parent_set(m_hoversel, win);
    elm_hoversel_label_set(m_hoversel, "Accounts");
    evas_object_size_hint_weight_set(m_hoversel, -1.0, 0.0);
    evas_object_size_hint_align_set(m_hoversel, -1.0, 0.0);
    elm_box_pack_end(m_content_box, m_hoversel);
    evas_object_show(m_hoversel);

    fillAccountHoversel();

    Evas_Object* spacerbx = elm_box_add(win);
    evas_object_size_hint_weight_set(spacerbx, 1.0, 1.0);
    evas_object_size_hint_align_set(spacerbx, -1.0, -1.0);
    elm_box_pack_end(m_content_box, spacerbx);
    evas_object_show(spacerbx);

    Evas_Object* horbx = elm_box_add(win);
    elm_box_horizontal_set(horbx, true);
    evas_object_size_hint_weight_set(horbx, 1.0, 0.0);
    evas_object_size_hint_align_set(horbx, -1.0, -1.0);
    elm_box_pack_end(m_content_box, horbx);
    evas_object_show(horbx);


    Evas_Object* close_bt = elm_button_add(win);
    elm_button_label_set(close_bt, "Close");
    evas_object_smart_callback_add(close_bt, "clicked", close_action, NULL);
    evas_object_size_hint_weight_set(close_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(close_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, close_bt);
    evas_object_show(close_bt);

    Evas_Object* connect_bt = elm_button_add(win);
    elm_button_label_set(connect_bt, "Connect");
    evas_object_smart_callback_add(connect_bt, "clicked", connect_action, this);
    evas_object_size_hint_weight_set(connect_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(connect_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, connect_bt);
    evas_object_show(connect_bt);



}

AccountWindow::~AccountWindow()
{
    //dtor
}


void AccountWindow::fillAccountHoversel(){

    std::list<Account*> acclist = ConfigManager::getInstance()->getAccountList();
    std::list<Account*>::iterator it;
    for (it=acclist.begin(); it != acclist.end(); it++){
        Account* tempacc = (*it);

        CallbackParam* param = new CallbackParam();
        param->self = this;
        param->account = tempacc;

        std::string iconpath = ConfigManager::getInstance()->getLocation() + "share/vilari_mail/icons/account.png";

        elm_hoversel_item_add(m_hoversel,
                              tempacc->getName().c_str(),
                              iconpath.c_str(),
                              ELM_ICON_FILE,
                              hoverselection_action,
                              param);

        if(tempacc->isDefault()){
            elm_hoversel_label_set(m_hoversel, tempacc->getName().c_str());
            m_selected_account = tempacc;
        }
    }

}




void AccountWindow::hoverselection_action(void *data, Evas_Object *obj, void *event_info){
    CallbackParam* param = static_cast<CallbackParam*>(data);
    Account* tempacc = param->account;
    AccountWindow* self = param->self;
    elm_hoversel_label_set(self->m_hoversel, tempacc->getName().c_str());
    self->m_selected_account = tempacc;

}


void AccountWindow::close_action(void *data, Evas_Object *obj, void *event_info)
{
    Controller::getInstance()->quit();
}



void AccountWindow::connect_action(void *data, Evas_Object *obj, void *event_info){
    AccountWindow* self = static_cast<AccountWindow*>(data);
    Controller::getInstance()->connect(self->m_selected_account);


}


