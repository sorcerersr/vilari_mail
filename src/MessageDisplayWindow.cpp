/*
 *      MessageDisplayWindow.cpp
 *      
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *      
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MessageDisplayWindow.h"

MessageDisplayWindow::MessageDisplayWindow(Evas_Object* win)  : Content(win)
{
    Evas_Object* sc = elm_scroller_add(win);
    //elm_scroller_content_min_limit(sc, 1, 0);
    //elm_scroller_policy_set(sc, ELM_SCROLLER_POLICY_OFF, ELM_SCROLLER_POLICY_ON);
    evas_object_size_hint_weight_set(sc, 1.0, 1.0);
    evas_object_size_hint_align_set(sc, -1.0, -1.0);
    elm_box_pack_end(m_content_box, sc);
    evas_object_show(sc);

    elm_scroller_bounce_set(sc ,false, false);
    
    m_entry = elm_entry_add(win);

    elm_entry_entry_set(m_entry,"");
    elm_entry_line_wrap_set(m_entry, true);
    elm_entry_editable_set(m_entry, false);
    evas_object_size_hint_weight_set(m_entry, 1.0, 1.0);
    evas_object_size_hint_align_set(m_entry, -1.0, -1.0);
    elm_scroller_content_set(sc, m_entry);
    evas_object_show(m_entry);

    Evas_Object* horbx = elm_box_add(win);
    elm_box_horizontal_set(horbx, true);
    evas_object_size_hint_weight_set(horbx, 1.0, 0.0);
    evas_object_size_hint_align_set(horbx, -1.0, -1.0);
    elm_box_pack_end(m_content_box, horbx);
    evas_object_show(horbx);


    Evas_Object* close_bt = elm_button_add(win);
    elm_button_label_set(close_bt, "back");
    evas_object_smart_callback_add(close_bt, "clicked", back_action, NULL);
    evas_object_size_hint_weight_set(close_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(close_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, close_bt);
    evas_object_show(close_bt);

    m_action_bt = elm_button_add(win);
    elm_button_label_set(m_action_bt, "Actions");
    evas_object_smart_callback_add(m_action_bt, "clicked", actions_action, this);
    evas_object_size_hint_weight_set(m_action_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(m_action_bt, -1.0, -1.0);
    elm_box_pack_end(horbx, m_action_bt);
    evas_object_show(m_action_bt);
}

MessageDisplayWindow::~MessageDisplayWindow()
{
    //dtor
}


void MessageDisplayWindow::back_action(void *data, Evas_Object *obj, void *event_info){
    Controller::getInstance()->goBack();
}


void MessageDisplayWindow::updateMessageDisplay(Message* message){

    m_message = message;

    std::string messagetext = message->getText();
    messagetext = Util::str_replace(messagetext, "\r\n", "<br>");
    messagetext = Util::str_replace(messagetext, "\r", "<br>");

    Header* header = message->getHeader();

    std::ostringstream msgstream;
    msgstream << "<b>Subject:</b> " << header->getSubject() << "<br>";
    msgstream << "<b>Date:</b> " << header->getDate() << "<br>";
    msgstream << "<b>From</b>: ";

    std::list< Address* >::iterator it;

    std::list< Address* > fromlist = header->getFromList();
    for (it=fromlist.begin(); it != fromlist.end(); it++){
        msgstream << (*it)->getDisplayName() << " (" << (*it)->getAddress() << ")<b>;</b><br>";
    }
    if(fromlist.empty()){
        msgstream << "<br>";
    }


    std::list< Address* > tolist = header->getToList();
    msgstream << "<b>To:</b>: ";
    for (it=tolist.begin(); it != tolist.end(); it++){
        msgstream << (*it)->getDisplayName() << " (" << (*it)->getAddress() << ")<b>;</b><br>";
    }
    if(tolist.empty()){
        msgstream << "<br>";
    }


    std::list< Address* > cclist = header->getCCList();
    msgstream << "<b>Cc:</b>: ";
    for (it=cclist.begin(); it != cclist.end(); it++){
        msgstream << (*it)->getDisplayName() << " (" << (*it)->getAddress() << ")<b>;</b><br>";
    }
    if(cclist.empty()){
        msgstream << "<br>";
    }

    msgstream << "<b>Messagebody:</b>:<br>";
    msgstream << messagetext;

    elm_entry_entry_set(m_entry,msgstream.str().c_str());

}


void MessageDisplayWindow::actions_action(void *data, Evas_Object *obj, void *event_info){
    MessageDisplayWindow* self = static_cast<MessageDisplayWindow*>(data);
    self->showActionsHover();
}


void MessageDisplayWindow::readbutton_action(void *data, Evas_Object *obj, void *event_info){
    MessageDisplayWindow* self = static_cast<MessageDisplayWindow*>(data);
    Controller::getInstance()->toggleReadFlag(self->m_message->getHeader());
    evas_object_hide(self->m_hover);
    evas_object_del(self->m_hover);
}


void MessageDisplayWindow::showActionsHover(){

    m_hover = elm_hover_add(m_win);
    evas_object_size_hint_weight_set(m_hover, 1.0, 1.0);
    evas_object_size_hint_align_set(m_hover, 1.0, 1.0);
    evas_object_show(m_hover);

    Evas_Object* bx = elm_box_add(m_win);
    evas_object_size_hint_weight_set(bx, 1.0, 0.0);
    evas_object_size_hint_align_set(bx, 1.0, 1.0);
    evas_object_show(bx);

    elm_hover_content_set(m_hover, "top", bx);
    elm_hover_parent_set(m_hover, m_win);
    elm_hover_target_set(m_hover, m_action_bt);

    Evas_Object* move_bt = elm_button_add(m_win);
    elm_button_label_set(move_bt, "move message");
    //evas_object_smart_callback_add(move_bt, "clicked", close_action, NULL);
    evas_object_size_hint_weight_set(move_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(move_bt, -1.0, -1.0);
    elm_box_pack_end(bx, move_bt);
    evas_object_show(move_bt);

    Evas_Object* read_bt = elm_button_add(m_win);

    if(m_message->getHeader()->getReadFlag()){
        elm_button_label_set(read_bt, "mark as unread");
    } else {
        elm_button_label_set(read_bt, "mark as read");
    }
    evas_object_smart_callback_add(read_bt, "clicked", readbutton_action, this);
    evas_object_size_hint_weight_set(read_bt, 1.0, 0.0);
    evas_object_size_hint_align_set(read_bt, -1.0, -1.0);
    elm_box_pack_end(bx, read_bt);
    evas_object_show(read_bt);


}

