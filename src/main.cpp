/*
 *      main.cpp
 *
 *      Copyright 2009 Sorcerer <sorcerersr@mailbox.org>
 *
 *      This file is part of Vilari Mail.
 *
 *      Vilari Mail is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      (at your option) any later version.
 *
 *      Vilari Mail is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with Vilari Mail.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include <iostream>
#include "commons.h"
#include "IMAP.h"
#include <Controller.h>

using namespace std;

int main(int argc, char* argv[])
{
    // initialize Elementary
    elm_init(argc, argv);

    // initialize configuration
    ConfigManager::getInstance()->init(argv[0]);

    for(int i = 0; i < argc; i++){
        if((0 == std::strcmp(argv[i],"--debug")) || (0 == std::strcmp(argv[i],"-d")))  {
          std::cout << "Debugging output is now turned on" << std::endl;
          ConfigManager::getInstance()->setDebug(true);
        }

        if((0 == std::strcmp(argv[i],"--memdebug")) || (0 == std::strcmp(argv[i],"-m")))  {
          std::cout << "Memory Debugging output is now turned on (watch constructors and destructors)" << std::endl;
          ConfigManager::getInstance()->setMemDebug(true);
        }
    }

    Controller* controller = Controller::getInstance();
    controller->runApp();

    return 0;
}
